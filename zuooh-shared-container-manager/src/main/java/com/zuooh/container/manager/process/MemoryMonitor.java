package com.zuooh.container.manager.process;

public interface MemoryMonitor {
   String showHeapMemoryUsage();
   String showHeapMemoryTrend();
   String showProcessMemoryTrend();
   String showSharedMemoryTrend();
   String showPrivateMemoryTrend();
   void dumpMemory(String file);
}
