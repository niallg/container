package com.zuooh.container.manager.explore;

public interface ObjectFieldInfo {
	public ObjectInfo getObjectInfo();
	public Object getFieldValue();
	public Class getFieldClass();
	public String getFieldName();
}
