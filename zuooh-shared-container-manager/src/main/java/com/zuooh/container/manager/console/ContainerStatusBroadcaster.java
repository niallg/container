package com.zuooh.container.manager.console;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.process.ProcessMemory;
import com.zuooh.common.process.ProcessMonitor;
import com.zuooh.common.task.Task;
import com.zuooh.container.annotation.ManagedAttribute;
import com.zuooh.container.annotation.ManagedResource;
import com.zuooh.message.Message;
import com.zuooh.message.MessagePublisher;

@ManagedResource("Announces availability for management")
public class ContainerStatusBroadcaster implements Task {
   
   private static final Logger LOG = LoggerFactory.getLogger(ContainerStatusBroadcaster.class);

   private final MessagePublisher publisher;
   private final ProcessMonitor process;
   private final Principal source;   
   private final long frequency;
   
   public ContainerStatusBroadcaster(MessagePublisher publisher, ProcessMonitor process, Principal source) {
      this(publisher, process, source, 5000);
   }   
   
   public ContainerStatusBroadcaster(MessagePublisher publisher, ProcessMonitor process, Principal source, long frequency) {
      this.publisher = publisher;
      this.frequency = frequency;
      this.process = process;
      this.source = source;     
   }
   
   @ManagedAttribute("User name for this console")
   public String getUser() {
      return source.getName();
   }
   
   @Override
   public long executeTask() {
      String user = source.getName();
      ProcessMemory memory = process.getProcessMemory();
      String platform = process.getDevicePlatform();
      String version = process.getDeviceVersion();
      long privateMemory = memory.getPrivateMemory();
      long heapMemory = memory.getHeapMemory();
      long totalMemory = memory.getTotalMemory();
      long sharedMemory = memory.getSharedMemory();      
           
      if(memory != null) {
         ContainerStatusEvent event = new ContainerStatusEvent(user, platform, version, heapMemory, totalMemory, sharedMemory, privateMemory);
         Message message = new Message(event);
         
         try {
            publisher.publish(message);
         } catch(Exception e) {
            LOG.info("Could not publish announcement", e);
         }
      }
      return frequency;
   }

}
