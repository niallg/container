package com.zuooh.container.manager.console;

import java.security.Principal;

public class ContainerSource implements Principal {
   
   private final String source;
   
   public ContainerSource(String source) {
      this.source = source;
   }   
   
   @Override
   public String getName() {
      return source;
   } 
}
