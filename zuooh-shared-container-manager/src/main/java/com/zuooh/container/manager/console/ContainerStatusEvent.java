package com.zuooh.container.manager.console;

import java.io.Serializable;

public class ContainerStatusEvent implements Serializable {

   private static final long serialVersionUID = 1L;

   private final long privateMemory; 
   private final long heapMemory;
   private final long totalMemory;
   private final long sharedMemory;
   private final long announceTime;
   private final String platform;
   private final String version;
   private final String user;

   public ContainerStatusEvent(String user, String platform, String version, long heapMemory, long totalMemory, long sharedMemory, long privateMemory) {
      this.announceTime = System.currentTimeMillis();
      this.privateMemory = privateMemory;
      this.heapMemory = heapMemory;
      this.totalMemory = totalMemory;
      this.sharedMemory = sharedMemory;
      this.platform = platform;
      this.version = version;
      this.user = user;
   }
   
   public String getUser() {
      return user;
   }
   
   public String getVersion() {
      return version;
   }
   
   public String getPlatform() {
      return platform;
   }
   
   public long getAnnounceTime() {
      return announceTime;
   }

   public long getPrivateMemory() {
      return privateMemory;
   }

   public long getHeapMemory() {
      return heapMemory;
   }

   public long getTotalMemory() {
      return totalMemory;
   }

   public long getSharedMemory() {
      return sharedMemory;
   }
}
