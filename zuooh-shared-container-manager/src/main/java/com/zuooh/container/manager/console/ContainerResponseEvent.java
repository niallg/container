package com.zuooh.container.manager.console;

import java.io.Serializable;

public class ContainerResponseEvent implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private final String requestId;
   private final String response;
   private final String user;

   public ContainerResponseEvent(String user, String requestId, String response) {
      this.response = response;
      this.requestId = requestId;
      this.user = user;
   }

   public String getResponse() {
      return response;
   }
   
   public String getRequestId() {
      return requestId;
   }
   
   public String getUser() {
      return user;
   }
}
