package com.zuooh.container.manager.console;

import java.io.DataOutputStream;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.io.Base64OutputStream;
import com.zuooh.message.Message;
import com.zuooh.message.MessageListener;
import com.zuooh.message.MessagePublisher;

public class ContainerMessageListener implements MessageListener {
   
   private static final Logger LOG = LoggerFactory.getLogger(ContainerMessageListener.class);

   private final ContainerConsoleInvoker bridge;
   private final MessagePublisher publisher;
   
   public ContainerMessageListener(ContainerConsoleInvoker bridge, MessagePublisher publisher) {
      this.publisher = publisher;
      this.bridge = bridge;
   }   
   
   public void onRequest(ContainerRequestEvent request) {
      String requestId = request.getRequestId();
      String path = request.getPath();
      String user = request.getUser();
      Map<String, String> query = request.getQuery();
      String body = bridge.executeRequest(path, query); // here we actually go in to the container console
      
      if(body != null) {         
         try {
            Base64OutputStream encoder = new Base64OutputStream();
            GZIPOutputStream compressor = new GZIPOutputStream(encoder);
            DataOutputStream writer = new DataOutputStream(compressor);
            
            writer.writeUTF(body);
            writer.flush();
            writer.close();
            
            String encoded = encoder.toString();
            
            ContainerResponseEvent response = new ContainerResponseEvent(user, requestId, encoded);
            Message responseMessage = new Message(response);
            
            publisher.publish(responseMessage);
         } catch(Exception e) {
            LOG.info("Could not publish response", e);
         }
      }
   }

   @Override
   public void onMessage(Message message) {
      Object value = message.getValue();
      
      if(value instanceof ContainerRequestEvent) {
         onRequest((ContainerRequestEvent)value);
      }
   }

   @Override
   public void onException(Exception cause) {
      LOG.info("Got exception", cause);
   }

   @Override
   public void onHeartbeat() {
      LOG.info("Got heartbeat");
   }

   @Override
   public void onReset() {
      LOG.info("Got reset"); 
   }
}
