package com.zuooh.container.manager.console;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.container.Container;
import com.zuooh.container.annotation.ManagedResource;

public class ComponentResolver {

   private final Map<String, Method> operations;
   private final PropertyMapper mapper;
   private final Container container;

   public ComponentResolver(Container container) {
      this.operations = new ConcurrentHashMap<String, Method>();
      this.mapper = new PropertyMapper();
      this.container = container;
   }

   public Map<ManagedResource, Method> resolveResources(Object instance) {
      Class type = instance.getClass();
      Method[] componentMethods = type.getDeclaredMethods();

      if(componentMethods.length > 0) {
         Map<ManagedResource, Method> components = new HashMap<ManagedResource, Method>();

         for(Method method : componentMethods) {
            ManagedResource methodResource = method.getAnnotation(ManagedResource.class);

            if(methodResource != null) {
               try {
                  Class[] parameterTypes = method.getParameterTypes();
                  
                  if(!method.isAccessible()) {
                     method.setAccessible(true);
                  }
                  Object returnValue = method.invoke(instance);
                  Class<?> returnType = returnValue.getClass();
                  ManagedResource returnResource = returnType.getAnnotation(ManagedResource.class);

                  if(parameterTypes.length == 0 && returnResource != null) {
                     components.put(methodResource, method);
                  }
               } catch(Exception e) {
                  throw new IllegalStateException("Problem getting resource from " + method, e);
               }
            }
         }
         return components;
      }
      return Collections.emptyMap();
   }

   public List<Method> resolveMethods(Object instance) {
      LinkedList<Method> methods = new LinkedList<Method>();

      if(instance != null) {
         Class type = instance.getClass();

         while(type != null) {
            Method[] componentMethods = type.getDeclaredMethods();

            for(Method method : componentMethods) {
               methods.addFirst(method);
            }
            type = type.getSuperclass();
         }
      }
      return methods;
   }

   public Object resolveComponent(String componentPath) {
      String[] pathParts = componentPath.split("\\.");
      Object instance = container.lookup(pathParts[0]);

      if(pathParts.length > 1) {
         for(int i = 1; i < pathParts.length; i++) {
            try {
               Class type = instance.getClass();
               Method method = mapper.getProperty(type, pathParts[i]);

               if(method == null) {
                  method = type.getDeclaredMethod(pathParts[i]);
               }
               instance = method.invoke(instance);
            } catch(Exception e) {
               throw new IllegalStateException("Could not resolve component " + componentPath, e);
            }
         }
      }
      return instance;
   }

   public Method resolveMethod(String methodPath) {
      if(operations.isEmpty()) {
         Method[] callableMethods = ContainerConsole.class.getDeclaredMethods();

         for(Method method : callableMethods) {
            Class returnType = method.getReturnType();
            Class[] parameterTypes = method.getParameterTypes();

            if(parameterTypes.length == 2 && returnType == void.class) {
               if(parameterTypes[0] == ContainerRequest.class && parameterTypes[1] == ContainerResponse.class) {
                  String name = method.getName();
                  operations.put("/" + name, method);
               }
            }
         }
      }
      return operations.get(methodPath);
   }
}
