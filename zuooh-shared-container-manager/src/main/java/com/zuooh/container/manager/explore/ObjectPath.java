package com.zuooh.container.manager.explore;

import java.util.List;

public interface ObjectPath {
	public String getObjectName();
	public List<ObjectId> getObjectPath();
	public ObjectPath getRelativePath(ObjectId objectId);
}
