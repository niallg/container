package com.zuooh.container.manager.process;

import java.util.Set;

import com.zuooh.common.FileSystem;
import com.zuooh.common.thread.ThreadDumper;
import com.zuooh.common.time.DateTime;
import com.zuooh.container.Container;
import com.zuooh.container.annotation.ManagedAttribute;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;

@ManagedResource("Agent for the application")
public class ProcessAgent {

   private final ThreadDumper threadDumper;
   private final FileSystem fileSystem;
   private final Container container;
   private final String name;

   public ProcessAgent(FileSystem fileSystem, Container container, String name) {
      this.threadDumper = new ThreadDumper();
      this.fileSystem = fileSystem;
      this.container = container;
      this.name = name;
   }

   @ManagedAttribute("Free external file space")
   public String getFreeSpace() {
      long space = fileSystem.freeExternalSpace();

      if(space > 0) {
         return getSize(space);
      }
      return "0B";
   }

   @ManagedAttribute("Memory used by the application")
   public String getMemoryUsage() {
      double totalMemory = Runtime.getRuntime().totalMemory();
      double freeMemory = Runtime.getRuntime().freeMemory();
      double usedMemory = totalMemory - freeMemory;
      double percentage = (usedMemory / totalMemory) * 100.0d;

      return Math.floor(percentage) + "%";
   }
   
   @ManagedOperation("Force garbage collection")
   public void collectGarbage() {
      System.gc();
   }

   @ManagedOperation("Dump the current threads")
   public String dumpThreads() {
      return threadDumper.dumpThreads();
   }

   @ManagedOperation("List external files in the application")
   public String listExternalFiles() {
      return listExternalFiles("");
   }

   @ManagedOperation("List external files in the application")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="path", description="Path to list files in")
   })
   public String listExternalFiles(String path) {
      String[] list = fileSystem.listExternalFiles(path);
      long octets = fileSystem.sizeOfExternalFile(path);      
      String size = getSize(octets);

      if(list.length > 0) {
         StringBuilder builder = new StringBuilder();

         builder.append("<h2>./");
         builder.append(path);         
         builder.append(" ("); 
         builder.append(size);
         builder.append(")");         
         builder.append("</h2>");
         builder.append("<table border='1'>");
         builder.append("<th>File</th>");
         builder.append("<th>Size</th>");
         builder.append("<th>Modified</th>");
         builder.append("<th colspan='2'>Action</th>");

         for(String file : list) {
            String filePath = file;
            
            if(!filePath.startsWith(path)) {
               filePath = path + "/" + file;
            }
            long fileModified = fileSystem.lastExternalModified(filePath);
            long fileOctets = fileSystem.sizeOfExternalFile(filePath);
            String fileSize = getSize(fileOctets);
            DateTime fileTime = DateTime.at(fileModified);

            builder.append("<tr>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=listExternalFiles&signature=1&path=");
            builder.append(filePath);
            builder.append("'>");
            builder.append(file);
            builder.append("</a>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append(fileSize);
            builder.append("</td>");
            builder.append("<td>");
            builder.append(fileTime);
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=deleteExternalFile&signature=1&path=");
            builder.append(filePath);
            builder.append("'>Delete</a>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=sendExternalFile&signature=1&path=");
            builder.append(filePath);
            builder.append("'>Send</a>");
            builder.append("</td>");
            builder.append("</tr>");
         }
         builder.append("</table>");
         return builder.toString();
      }
      return null;
   }

   @ManagedOperation("Delete an external file")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="path", description="Path to the file")
   })
   public boolean deleteExternalFile(String path) {
      return fileSystem.deleteExternalFile(path);
   }

   @ManagedOperation("List files in the application")
   public String listFiles() {
      return listFiles("");
   }

   @ManagedOperation("List files in the application")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="path", description="Path to list files in")
   })
   public String listFiles(String path) {
      String[] list = fileSystem.listFiles(path);

      if(list.length > 0) {
         StringBuilder builder = new StringBuilder();

         builder.append("<h2>./");
         builder.append(path);
         builder.append("</h2>");
         builder.append("<table border='1'>");
         builder.append("<th>File</th>");
         builder.append("<th>Modified</th>");
         builder.append("<th colspan='2'>Action</th>");

         for(String file : list) {
            String filePath = file;
            
            if(!file.startsWith(path)) {
               filePath = path + "/" + file;
            }
            long fileModified = fileSystem.lastModified(filePath);
            DateTime fileTime = DateTime.at(fileModified);

            builder.append("<tr>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=listFiles&signature=1&path=");
            builder.append(filePath);
            builder.append("'>");
            builder.append(file);
            builder.append("</a>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append(fileTime);
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=deleteFile&signature=1&path=");
            builder.append(filePath);
            builder.append("'>Delete</a>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<a href='operationInvoked?component=");
            builder.append(name);
            builder.append("&operation=sendFile&signature=1&path=");
            builder.append(filePath);
            builder.append("'>Send</a>");
            builder.append("</td>");            
            builder.append("</tr>");
         }
         builder.append("</table>");
         return builder.toString();
      }
      return null;
   }

   @ManagedOperation("Delete a file")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="path", description="Path to the file")
   })
   public boolean deleteFile(String path) {
      return fileSystem.deleteFile(path);
   }

   @ManagedOperation("List all the components in the container")
   public String listAllComponents() {
      Set<String> list = container.listAll();

      if(!list.isEmpty()) {
         StringBuilder builder = new StringBuilder();

         builder.append("<table border='1'>");
         builder.append("<th>Component</th>");

         for(String name : list) {
            builder.append("<tr>");
            builder.append("<td>").append(name).append("</td>");
            builder.append("</tr>");
         }
         builder.append("</table>");
         return builder.toString();
      }
      return null;
   }

   private String getSize(long size) {
      if(size < 1024) {
         return size + "B";
      }
      if(size < 1024 * 1024) {
         return size / 1024 + "K";
      }
      if(size < 1024 * 1024 * 1024) {
         return size / (1024 * 1024) + "M";
      }
      return size / (1024 * 1024 * 1024) + "G";
   }
}
