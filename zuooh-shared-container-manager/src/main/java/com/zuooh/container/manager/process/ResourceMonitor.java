package com.zuooh.container.manager.process;

public interface ResourceMonitor {
   String showResourceSummary();
   String showResources(String filter);
   String showResources();
}
