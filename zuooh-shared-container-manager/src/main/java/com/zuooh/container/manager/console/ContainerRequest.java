package com.zuooh.container.manager.console;

import java.util.Map;

public class ContainerRequest {

   private final Map<String, String> parameters;
   private final String prefix;
   private final String path;

   public ContainerRequest(String path, String prefix, Map<String, String> parameters) {
      this.parameters = parameters;
      this.prefix = prefix;
      this.path = path;
   }

   public String getActionPath() {
      int length = prefix.length();

      if(path.startsWith(prefix)) {
         return path.substring(length);
      }
      return path;
   }

   public String getParameter(String name) {
      return parameters.get(name);
   }

   public String getPath(){
      return path;
   }
}
