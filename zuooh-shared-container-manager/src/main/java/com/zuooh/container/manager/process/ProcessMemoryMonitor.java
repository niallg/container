package com.zuooh.container.manager.process;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.process.ProcessMemory;
import com.zuooh.common.process.ProcessMonitor;
import com.zuooh.common.thread.ThreadPoolFactory;
import com.zuooh.container.annotation.ManagedAttribute;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;

@ManagedResource("Monitors memory usage")
public class ProcessMemoryMonitor implements MemoryMonitor {
   
   private static final Logger LOG = LoggerFactory.getLogger(ProcessMemoryMonitor.class);

   private final BlockingQueue<MemorySample> samples;
   private final AtomicReference<Object> leak;
   private final ThreadFactory factory;
   private final MemorySampler sampler;
   private final ProcessMonitor process;

   public ProcessMemoryMonitor(ProcessMonitor process) {
      this.factory = new ThreadPoolFactory(MemorySampler.class);
      this.samples = new LinkedBlockingQueue<MemorySample>();
      this.leak = new AtomicReference<Object>();
      this.sampler = new MemorySampler();
      this.process = process;
   }

   @ManagedAttribute("The maximum size of the heap")
   public String getMaxHeapMemory() {
      Runtime runtime = Runtime.getRuntime();
      long maxMemory = runtime.maxMemory();

      return getSize(maxMemory);
   }

   @ManagedAttribute("The current total size of the heap")
   public String getTotalHeapMemory() {
      Runtime runtime = Runtime.getRuntime();
      long totalMemory = runtime.totalMemory();

      return getSize(totalMemory);
   }

   @ManagedAttribute("The total size of used heap memory")
   public String getFreeHeapMemory() {
      Runtime runtime = Runtime.getRuntime();
      long freeMemory = runtime.freeMemory();

      return getSize(freeMemory);
   }

   @ManagedAttribute("The total process memory")
   public String getProcessTotalMemory() {
      ProcessMemory memory = process.getProcessMemory();
      long total = memory.getTotalMemory();

      return getSize(total);
   }

   @ManagedAttribute("The process shared memory")
   public String getProcessSharedMemory() {
      ProcessMemory memory = process.getProcessMemory();
      long shared = memory.getSharedMemory();

      return getSize(shared);
   }

   @ManagedAttribute("The process private memory")
   public String getProcessPrivateMemory() {
      ProcessMemory memory = process.getProcessMemory();
      long priv = memory.getPrivateMemory();

      return getSize(priv);
   }

   @ManagedOperation("Show the current memory usage")
   public String showHeapMemoryUsage() {
      StringBuilder builder = new StringBuilder();

      builder.append("<table border='1'>\n");
      builder.append("<th>estimate</th>\n");
      builder.append("<th>maximum</th>\n");
      builder.append("<th>total</th>\n");
      builder.append("<th>used</th>\n");
      builder.append("<th>free</th>\n");
      builder.append("<th>scale</th>\n");

      ProcessMemory memory = process.getProcessMemory();
      Runtime runtime = Runtime.getRuntime();
      double freeMemory = runtime.freeMemory();
      double totalMemory = runtime.totalMemory();
      double maxMemory = runtime.maxMemory();
      double heapEstimate = memory.getHeapMemory();
      long percentFree = Math.round((freeMemory / maxMemory) * 100.0);
      long percentUsed = Math.round(((totalMemory - freeMemory) / maxMemory) * 100.0);
      long percentRemaining = 100 - (percentFree + percentUsed);

      builder.append("<tr>\n");
      builder.append("<td>").append(getSize(heapEstimate)).append("</td>\n");
      builder.append("<td>").append(getSize(maxMemory)).append("</td>\n");
      builder.append("<td>").append(getSize(totalMemory)).append("</td>\n");
      builder.append("<td>").append(getSize(totalMemory - freeMemory)).append("</td>\n");
      builder.append("<td>").append(getSize(freeMemory)).append("</td>\n");
      builder.append("<td>\n");
      builder.append("<table border='1' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr width='400'>\n");
      builder.append("<td height='40' width='").append(4 * percentUsed).append("' bgcolor='#ff0000'/>\n");
      builder.append("<td height='40' width='").append(4 * percentFree).append("' bgcolor='#00ff00'/>\n");
      builder.append("<td height='40' width='").append(4 * percentRemaining).append("' bgcolor='#ffffff'/>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");

      return builder.toString();
   }

   @ManagedOperation("Show a historical trend of heap memory usage")
   public String showHeapMemoryTrend() {
      StringBuilder builder = new StringBuilder();

      String totalScale = getTotalHeapMemory();
      String maxScale = getMaxHeapMemory();

      builder.append("<table border='1'>\n");
      builder.append("<th>total (").append(totalScale).append(")</th>\n");
      builder.append("<th>maximum (").append(maxScale).append(")</th>\n");
      builder.append("<tr>\n");
      builder.append("<td valign='top'>\n");
      builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr>\n");

      for(MemorySample sample : samples) {
         long percentUsed = sample.percentOfTotalUsed;
         long percentFree = 100 - sample.percentOfTotalUsed;

         builder.append("<td height='300' width='4' valign='top'>\n");
         builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
         builder.append("<tr height='").append(10 + (3 * percentFree)).append("'><td bgcolor='#00ff00'>&nbsp;</td></tr>\n");
         builder.append("<tr height='").append(10 + (3 * percentUsed)).append("'><td bgcolor='#ff0000'>&nbsp;</td></tr>\n");
         builder.append("</table>\n");
         builder.append("</td>\n");
      }
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("<td valign='top'>\n");
      builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr>\n");

      for(MemorySample sample : samples) {
         long percentUsed = sample.percentOfMaxUsed;
         long percentFree = 100 - sample.percentOfMaxUsed;

         builder.append("<td height='320' width='4' valign='top'>\n");
         builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
         builder.append("<tr height='").append(10 + (3 * percentFree)).append("'><td bgcolor='#ffffff'>&nbsp;</td></tr>\n");
         builder.append("<tr height='").append(10 + (3 * percentUsed)).append("'><td bgcolor='#0000ff'>&nbsp;</td></tr>\n");
         builder.append("</table>\n");
         builder.append("</td>\n");
      }
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");

      return builder.toString();
   }

   @ManagedOperation("Show a historical trend of process memory usage")
   public String showProcessMemoryTrend() {
      StringBuilder builder = new StringBuilder();
      double maximumMemory = 0;

      for(MemorySample sample : samples) {
         if(sample.processMemory > maximumMemory) {
            maximumMemory = sample.processMemory;
         }
      }
      String scale = getSize(maximumMemory);

      builder.append("<table border='1'>\n");
      builder.append("<th>process (").append(scale).append(")</th>\n");
      builder.append("<tr>\n");
      builder.append("<td valign='top'>\n");
      builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr>\n");

      for(MemorySample sample : samples) {
         long percentUsed = Math.round((sample.processMemory / maximumMemory) * 100.0);
         long percentFree = 100 - percentUsed;

         builder.append("<td height='320' width='4' valign='top'>\n");
         builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
         builder.append("<tr height='").append(10 + (3 * percentFree)).append("'><td bgcolor='#00ff00'>&nbsp;</td></tr>\n");
         builder.append("<tr height='").append(10 + (3 * percentUsed)).append("'><td bgcolor='#ff0000'>&nbsp;</td></tr>\n");
         builder.append("</table>\n");
         builder.append("</td>\n");
      }
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");

      return builder.toString();
   }

   @ManagedOperation("Show a historical trend of shared memory usage")
   public String showSharedMemoryTrend() {
      StringBuilder builder = new StringBuilder();
      double maximumMemory = 0;

      for(MemorySample sample : samples) {
         if(sample.processMemory > maximumMemory) {
            maximumMemory = sample.processMemory;
         }
      }
      String scale = getSize(maximumMemory);
      builder.append("<table border='1'>\n");
      builder.append("<th>shared (").append(scale).append(")</th>\n");
      builder.append("<tr>\n");
      builder.append("<td valign='top'>\n");
      builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr>\n");

      for(MemorySample sample : samples) {
         long percentUsed = Math.round((sample.sharedMemory / maximumMemory) * 100.0);
         long percentFree = 100 - percentUsed;

         builder.append("<td height='320' width='4' valign='top'>\n");
         builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
         builder.append("<tr height='").append(10 + (3 * percentFree)).append("'><td bgcolor='#00ff00'>&nbsp;</td></tr>\n");
         builder.append("<tr height='").append(10 + (3 * percentUsed)).append("'><td bgcolor='#ff0000'>&nbsp;</td></tr>\n");
         builder.append("</table>\n");
         builder.append("</td>\n");
      }
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");

      return builder.toString();
   }

   @ManagedOperation("Show a historical trend of private memory usage")
   public String showPrivateMemoryTrend() {
      StringBuilder builder = new StringBuilder();
      double maximumMemory = 0;

      for(MemorySample sample : samples) {
         if(sample.processMemory > maximumMemory) {
            maximumMemory = sample.processMemory;
         }
      }
      String scale = getSize(maximumMemory);
      builder.append("<table border='1'>\n");
      builder.append("<th>private (").append(scale).append(")</th>\n");
      builder.append("<tr>\n");
      builder.append("<td valign='top'>\n");
      builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
      builder.append("<tr>\n");

      for(MemorySample sample : samples) {
         long percentUsed = Math.round((sample.privateMemory / maximumMemory) * 100.0);
         long percentFree = 100 - percentUsed;

         builder.append("<td height='320' width='4' valign='top'>\n");
         builder.append("<table border='0' cellpadding='0' cellspacing='0'>\n");
         builder.append("<tr height='").append(10 + (3 * percentFree)).append("'><td bgcolor='#00ff00'>&nbsp;</td></tr>\n");
         builder.append("<tr height='").append(10 + (3 * percentUsed)).append("'><td bgcolor='#ff0000'>&nbsp;</td></tr>\n");
         builder.append("</table>\n");
         builder.append("</td>\n");
      }
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");
      builder.append("</table>\n");
      builder.append("</td>\n");
      builder.append("</tr>\n");
      builder.append("</table>\n");

      return builder.toString();
   }
   
   @ManagedOperation("Leak some memory from the heap")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="size", description="Bytes to leak")
   })
   public void leakMemory(int size) {
      byte[] memory = new byte[size];
      leak.set(memory);            
   }

   @ManagedOperation("Dump memory to a file")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="file", description="Location to dump to")
   })
   public void dumpMemory(String file) {
      ProcessMemory memory = process.getProcessMemory();
      memory.dumpMemory(file);
   }

   @ManagedOperation("Sample memory usage for trend")
   public void sampleMemory() {
      Thread thread = factory.newThread(sampler);
      thread.start();
   }

   private String getSize(double size) {
      long rounded = Math.round(size);

      if(rounded < 1024) {
         return rounded + "B";
      }
      if(rounded < 1024 * 1024) {
         return rounded / 1024 + "K";
      }
      if(rounded < 1024 * 1024 * 1024) {
         return rounded / (1024 * 1024) + "M";
      }
      return rounded / (1024 * 1024 * 1024) + "G";
   }

   private class MemorySampler implements Runnable {
      
      public void run() {
         while(true) {
            try {
               Thread.sleep(1000);
               ProcessMemory memory = process.getProcessMemory();
               long processMemory = memory.getTotalMemory();
               long sharedMemory = memory.getSharedMemory();
               long privateMemory = memory.getPrivateMemory();
               Runtime runtime = Runtime.getRuntime();
               double maxMemory = runtime.maxMemory();
               double freeMemory = runtime.freeMemory();
               double totalMemory = runtime.totalMemory();
               long percentOfMaxUsed = Math.round((totalMemory / maxMemory) * 100.0);
               long percentOfTotalFree = Math.round((freeMemory / totalMemory) * 100.0);
               long percentOfTotalUsed = 100 - percentOfTotalFree;
               int size = samples.size();

               if(size > 200) {
                  samples.poll();
               }
               MemorySample sample = new MemorySample(percentOfTotalUsed, percentOfMaxUsed, processMemory, sharedMemory, privateMemory);
               samples.offer(sample);
            } catch(Exception e) {
               LOG.info("Error sampling memory", e);
            }
         }
      }
   }

   private class MemorySample {

      public final long processMemory;
      public final long sharedMemory;
      public final long privateMemory;
      public final long percentOfMaxUsed;
      public final long percentOfTotalUsed;

      public MemorySample(long percentOfTotalUsed, long percentOfMaxUsed, long processMemory, long sharedMemory, long privateMemory) {
         this.percentOfTotalUsed = percentOfTotalUsed;
         this.percentOfMaxUsed = percentOfMaxUsed;
         this.processMemory = processMemory;
         this.sharedMemory = sharedMemory;
         this.privateMemory = processMemory;
      }

   }
}
