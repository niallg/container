package com.zuooh.container.manager.console;

import java.io.Serializable;
import java.util.Map;

public class ContainerRequestEvent implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private final Map<String, String> query;
   private final String requestId;
   private final String path;
   private final String user;
   private final long requestTime;

   public ContainerRequestEvent(String user, String path, String requestId, Map<String, String> query) {
      this.requestTime = System.currentTimeMillis();
      this.requestId = requestId;
      this.path = path;
      this.query = query;
      this.user = user;
   }
   
   public long getRequestTime() {
      return requestTime;
   }
   
   public String getUser() {
      return user;
   }
   
   public String getRequestId() {
      return requestId;
   }
   
   public String getPath() {
      return path;
   }

   public Map<String, String> getQuery() {
      return query;
   }
}
