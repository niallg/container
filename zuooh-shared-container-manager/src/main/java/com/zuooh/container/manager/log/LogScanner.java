package com.zuooh.container.manager.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zuooh.common.collections.FixedLengthQueue;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;
import com.zuooh.log.LogEvent;
import com.zuooh.log.LogFormatter;
import com.zuooh.log.LogLevel;
import com.zuooh.log.LogListener;

@ManagedResource("Scanner for recent log events")
public class LogScanner implements LogListener {

   private final FixedLengthQueue<LogEvent> queue;

   public LogScanner() {
      this(100);
   }
   
   public LogScanner(int capacity) {
      this.queue = new FixedLengthQueue<LogEvent>(capacity);
   }
   
   @ManagedOperation("List recent events")
   public synchronized String listEvents() {
      return listEvents(".*");
   }
   
   @ManagedOperation("List recent events")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="term", description="Term to filter with")
   })
   public synchronized String listEvents(String term) {  
      StringWriter buffer = new StringWriter();
      PrintWriter writer = new PrintWriter(buffer);            
     
      writer.print("<h3>");
      writer.print("Log events matching '");
      writer.print(term);
      writer.print("'");
      writer.print("</h3>");
      writer.print("<pre>");  
      
      if(!queue.isEmpty()) {
         Pattern pattern = Pattern.compile(term, Pattern.DOTALL);
         
         for(LogEvent event : queue) {
            long time = event.getTime();
            String thread = event.getThread();
            String message = event.getMessage();
            Matcher matcher = pattern.matcher(message);
            
            if(message.contains(term) || matcher.matches()) {
               Throwable throwable = event.getThrowable();
               LogLevel level = event.getLevel();      
               String date = LogFormatter.formatLongDate(time);         
                   
               writer.print(level);
               writer.print(" ");
               writer.print(date);
               writer.print(": ");
               writer.print("[");
               writer.print(thread);
               writer.print("] ");
               writer.println(message);           
               
               if(throwable != null) {
                  throwable.printStackTrace(writer);
               } 
            }
         }
      }
      writer.print("</pre>");
      
      return buffer.toString();
   }
   
   @ManagedOperation("Clear recent events")
   public synchronized void clearEvents() {
      queue.clear();
   }

   @Override
   public synchronized void log(LogEvent event) {
      queue.offer(event);
   }
}
