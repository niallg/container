package com.zuooh.container.manager.console;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import com.zuooh.container.annotation.ManagedResource;

@ManagedResource("Client that can manage a container")
public class ContainerConsoleInvoker {

   private final ContainerConsole console;
   private final String prefix;

   public ContainerConsoleInvoker(ContainerConsole console, String prefix) {
      this.console = console;
      this.prefix = prefix;
   }

   public String executeRequest(String path, Map<String, String> query) {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      OutputStreamWriter adapter = new OutputStreamWriter(buffer);
      PrintWriter writer = new PrintWriter(adapter);
      ContainerResponse containerResponse = new ContainerResponse(writer);
      ContainerRequest containerRequest = new ContainerRequest(path, prefix, query);

      try {
         console.process(containerRequest, containerResponse);
         writer.flush();
         writer.close();
      } catch(Exception e){
         StringWriter builder = new StringWriter();
         PrintWriter formatter = new PrintWriter(builder);

         formatter.print("<html><body>");
         formatter.print("<h1>Managed Resource Bridge Error</h1>");
         formatter.print("<pre>");
         e.printStackTrace(formatter);
         formatter.println("</pre>");
         formatter.println("</body></html>");
         formatter.close();
         return builder.toString();
      }
      try {
         return buffer.toString("UTF-8");
      } catch(Exception e) {
         return buffer.toString();
      }
   }
}
