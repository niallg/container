package com.zuooh.container.manager.console;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zuooh.container.Container;
import com.zuooh.container.annotation.ManagedAttribute;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;
import com.zuooh.container.manager.explore.ObjectIntrospector;

public class ContainerConsole {

   private final ObjectIntrospector introspector;
   private final ComponentExplorer explorer;
   private final ComponentResolver resolver;
   private final Container container;
   private final String color;

   public ContainerConsole(Container container, String color) {
      this.resolver = new ComponentResolver(container);
      this.introspector = new ContainerObjectIntrospector(resolver);
      this.explorer = new ComponentExplorer(introspector);
      this.container = container;
      this.color = color;
   }

   public void process(ContainerRequest request, ContainerResponse response) throws Exception {
      String methodCalled = request.getActionPath();
      Method method = resolver.resolveMethod(methodCalled);

      try {
         PrintWriter writer = response.getBody();

         writer.println("<html>");
         writer.println("<head>");
         writer.println("<title>Management Console</title>");
         writer.println("</head>");
         writer.print("<body bgcolor='");
         writer.print(color);
         writer.println("'>");

         if(method == null) {
            landingPage(request, response);
         } else {
            method.invoke(this, request, response);
         }
         writer.println("</body>");
         writer.println("</html>");
      } catch(Exception e) {
         throw new IllegalStateException("Could not process method " + methodCalled, e);
      }
   }

   public void landingPage(ContainerRequest request, ContainerResponse response) throws Exception {
      Set<String> matchingComponents = container.listMatches(ManagedResource.class);
      PrintWriter writer = response.getBody();

      writer.println("<h1>Management Console</h1>");
      writer.println("<hr>");
      writer.println("<table border='1'>");
      writer.println("<th>Name</th>");
      writer.println("<th>Type</th>");
      writer.println("<th>Description</th>");

      for(String component : matchingComponents) {
         Object instance = container.lookup(component);
         Class<?> type = instance.getClass();
         ManagedResource resource = type.getAnnotation(ManagedResource.class);
         String className = type.getName();
         String description = resource.value();

         writer.println("<tr>");
         writer.println("<td><a href='componentSelected?component=" +component + "'>" + component + "</a></td>");
         writer.println("<td>" + className + "</td>");
         writer.println("<td>" + description + "</td>");
         writer.println("</tr>");
      }
      writer.println("</table>");
   }

   public void componentSelected(ContainerRequest request, ContainerResponse response) throws Exception {
      String componentSelected = request.getParameter("component");
      String objectLink = explorer.getObjectLink(componentSelected);
      Object instance = resolver.resolveComponent(componentSelected);
      Map<ManagedResource, Method> resources = resolver.resolveResources(instance);
      List<Method> componentMethods = resolver.resolveMethods(instance);
      Class type = instance.getClass();
      PrintWriter writer = response.getBody();
      String className = type.getName();

      writer.println("<h2>");
      writer.println(componentSelected);
      writer.println("</h2>");
      writer.println("<hr>");
      writer.println("<ul>");
      writer.println("<li><a href='landingPage'>Show Components</a></li>");
      writer.println("<li><a href='" + objectLink + "'>Look Inside</a></li>");
      writer.println("</ul>");
      writer.println("<br>");
      writer.println("<hr>");
      writer.println("<table border='1'>");
      writer.println("<th>Attribute</th>");
      writer.println("<th>Value</th>");
      writer.println("<th>Description</th>");
      writer.println("<tr>");
      writer.println("<td>Class</td>");
      writer.println("<td>" + className + "</td>");
      writer.println("<td>Component class</td>");
      writer.println("</tr>");

      for(Method method : componentMethods) {
         ManagedAttribute attribute = method.getAnnotation(ManagedAttribute.class);
         Class[] parameterTypes = method.getParameterTypes();

         if(attribute != null && parameterTypes.length == 0) {
            String description = attribute.value();
            String methodName = method.getName();
            String propertyName = methodName;

            if(propertyName.startsWith("get")) {
               propertyName = propertyName.substring(3);
            }
            if(propertyName.startsWith("is")) {
               propertyName = propertyName.substring(2);
            }
            writer.println("<tr>");
            writer.println("<td>" + propertyName + "</td>");
            writer.println("<td>" + method.invoke(instance) + "</td>");
            writer.println("<td>" + description + "</td>");
            writer.println("</tr>");
         }
      }
      writer.println("<table>");
      writer.println("<hr>");

      if(!resources.isEmpty()) {
         Set<ManagedResource> annotations = resources.keySet();

         writer.println("<table border='1'>");
         writer.println("<th>Name</th>");
         writer.println("<th>Type</th>");
         writer.println("<th>Description</th>");

         for(ManagedResource resource : annotations) {
            Method method = resources.get(resource);
            Class returnType = method.getReturnType();
            String returnTypeName = returnType.getName();
            String displayName = returnType.getSimpleName();
            String description = resource.value();
            String methodName = method.getName();
            String propertyName = methodName;

            if(propertyName.startsWith("get")) {
               propertyName = propertyName.substring(3);
               displayName = propertyName;
            }
            if(propertyName.startsWith("is")) {
               propertyName = propertyName.substring(2);
               displayName = propertyName;
            }
            writer.println("<tr>");
            writer.println("<td><a href='componentSelected?component=" + componentSelected + "." + propertyName + "'>" + displayName + "</a></td>");
            writer.println("<td>" + returnTypeName + "</td>");
            writer.println("<td>" + description + "</td>");
            writer.println("</tr>");
         }
         writer.println("<table>");
         writer.println("<hr>");
      }

      for(Method method : componentMethods) {
         ManagedOperation operation = method.getAnnotation(ManagedOperation.class);
         Class[] parameterTypes = method.getParameterTypes();

         if(operation != null) {
            String description = operation.value();
            String methodName = method.getName();
            Class[] signature = method.getParameterTypes();

            writer.println("<h3>" + description + "</h3>");
            writer.println("<form method='POST' action='operationInvoked?component=" + componentSelected + "&operation=" + methodName + "&signature=" + signature.length + "'>");
            writer.println("<table border='1'>");
            writer.println("<tr>");
            writer.println("<td><input type='submit' value='" + methodName + "' name='" + methodName + "'/></td>");

            if(parameterTypes.length > 0) {
               ManagedOperationParameters parameters = method.getAnnotation(ManagedOperationParameters.class);
               ManagedOperationParameter[] parameterNames = parameters.value();

               writer.println("<td>");
               writer.println("<table>");

               if(parameterNames.length != parameterTypes.length) {
                  writer.println("<b>WARNING: Parameter types for this method are not annotated correctly</b>");
               }
               for(int i = 0; i < parameterNames.length; i++) {
                  writer.println("<tr>");
                  writer.println("<td><b>" + parameterNames[i].name() + ": </b></td>");
                  writer.println("<td><input type='text' name='" + parameterNames[i].name() + "'/></td>");
                  writer.println("<td><i>" + parameterNames[i].description() + "</i></td>");
                  writer.println("</tr>");
               }
               writer.println("</table>");
               writer.println("</td>");
               writer.println("</tr>");
            }
            writer.println("</table>");
            writer.println("</form>");
            writer.println("</hr>");
         }
      }
   }

   public void operationInvoked(ContainerRequest request, ContainerResponse response) throws Exception {
      String componentSelected = request.getParameter("component");
      String operationInvoked = request.getParameter("operation");
      String signatureLength = request.getParameter("signature");
      Object instance = resolver.resolveComponent(componentSelected);
      Class type = instance.getClass();
      Method[] declaredMethods = type.getDeclaredMethods();
      Method selectedMethod = declaredMethods[0];

      for(Method declaredMethod : declaredMethods) {
         ManagedOperation operation = declaredMethod.getAnnotation(ManagedOperation.class);

         if(operation != null) {
            String name = declaredMethod.getName();
            Class[] signature = declaredMethod.getParameterTypes();
            String lengthValue = String.valueOf(signature.length);

            if(operationInvoked.equals(name) && signatureLength.equals(lengthValue)) {
               selectedMethod = declaredMethod;
               break;
            }
         }
      }
      ManagedOperationParameters parameters = selectedMethod.getAnnotation(ManagedOperationParameters.class);
      PrintWriter writer = response.getBody();

      writer.println("<h2>");
      writer.println(componentSelected + "." + operationInvoked + "()");
      writer.println("</h2>");
      writer.println("<hr>");
      writer.println("<ul>");
      writer.println("<li><a href='landingPage'>Show Components</a></li>");
      writer.println("</ul>");
      writer.println("<br>");
      writer.println("<hr>");

      Class[] parameterTypes = selectedMethod.getParameterTypes();
      Object[] arguments = new Object[parameterTypes.length];

      if(parameterTypes.length > 0) {
         ManagedOperationParameter[] parameterNames = parameters.value();

         for(int i = 0; i < arguments.length; i++) {
            String name = parameterNames[i].name();
            String value = request.getParameter(name);

            if(parameterTypes[i] == int.class) {
               arguments[i] = Integer.parseInt(value);
            } else if(parameterTypes[i] == float.class){
               arguments[i] = Float.parseFloat(value);
            } else if(parameterTypes[i] == boolean.class) {
               arguments[i] = Boolean.parseBoolean(value);
            } else if(parameterTypes[i] == double.class) {
               arguments[i] = Double.parseDouble(value);
            } else if(parameterTypes[i] == long.class) {
               arguments[i] = Long.parseLong(value);
            } else if(parameterTypes[i] == String.class) {
               arguments[i] = value;
            }
         }
      }
      try {
         Object result = selectedMethod.invoke(instance, arguments);
         
         if(result != null) {
            writer.println(result);
         }
      } catch(Exception cause) {
         writer.println("<pre>");
         cause.printStackTrace(writer);
         writer.println("</pre>");
      }
   }

   public void exploreComponent(ContainerRequest request, ContainerResponse response) throws Exception {
      String objectPath = request.getParameter("path");
      PrintWriter writer = response.getBody();
      String componentName = explorer.getObjectName(objectPath);
      String details = explorer.getObjectInfo(objectPath);

      writer.println("<h2>");
      writer.println(componentName);
      writer.println("</h2>");
      writer.println("<hr>");
      writer.println("<ul>");
      writer.println("<li><a href='landingPage'>Show Components</a></li>");
      writer.println("</ul>");
      writer.println("<br>");
      writer.println("<hr>");
      writer.println(details);
   }



}
