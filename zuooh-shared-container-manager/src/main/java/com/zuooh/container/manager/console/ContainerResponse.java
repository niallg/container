package com.zuooh.container.manager.console;

import java.io.PrintWriter;

public class ContainerResponse {

   private PrintWriter body;

   public ContainerResponse(PrintWriter body) {
      this.body = body;
   }

   public PrintWriter getBody() {
      return body;
   }
}
