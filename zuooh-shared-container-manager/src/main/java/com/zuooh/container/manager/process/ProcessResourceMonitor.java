package com.zuooh.container.manager.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.zuooh.common.process.ProcessMonitor;
import com.zuooh.common.process.ProcessResource;
import com.zuooh.common.time.DateTime;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;

@ManagedResource("Monitors the allocated resources")
public class ProcessResourceMonitor implements ResourceMonitor {

   private final ProcessMonitor process;

   public ProcessResourceMonitor(ProcessMonitor process) {
      this.process = process;
   }

   @ManagedOperation("Show the a summary of allocated resources")
   public String showResourceSummary() {
      List<ProcessResource> resources = process.getProcessResources();

      if(resources != null) {
         StringBuilder builder = new StringBuilder();
         Map<Object, ProcessResource> table = new HashMap<Object, ProcessResource>();
         Map<Long, Object> sorted = new TreeMap<Long, Object>();
         double heapMaxSize = Runtime.getRuntime().maxMemory();
         double heapTotalSize = Runtime.getRuntime().totalMemory();
         double heapFreeMemory = Runtime.getRuntime().freeMemory();
         double heapUsedMemory = heapTotalSize - heapFreeMemory;
         long startTime = System.currentTimeMillis();
         long totalSize = 0;
         int count = 0;

         for(ProcessResource resource : resources) {
            Object uniqueKey = resource.getUniqueKey();
            long size = resource.getSizeEstimate();
            long time = resource.getAllocationTime();
            long key = (time * 1000) + count;
            
            if(time < startTime) {
               startTime = time;
            }
            table.put(uniqueKey, resource);
            sorted.put(key, uniqueKey);
            totalSize += size;
            count++;
         }
         String totalEstimate = getSize(totalSize);
         long totalPercentage = Math.round((totalSize / heapMaxSize) * 100.0f);
         long usedPercentage = Math.round((heapUsedMemory / heapMaxSize) * 100.0f);
         
         builder.append("<h2>");
         builder.append("Total ").append(count);
         builder.append(" (").append(totalEstimate).append(") using ");
         builder.append(totalPercentage);
         builder.append("% total used is ");
         builder.append(usedPercentage);
         builder.append("%</h2>");         
         builder.append("<table border='1'>");
         builder.append("<th>uniqueKey</th>");
         builder.append("<th>name</th>");
         builder.append("<th>owner</th>");
         builder.append("<th>thread</th>");
         builder.append("<th>time</th>");
         builder.append("<th>timeElapsed</th>");         
         builder.append("<th>sizeEstimate</th>");
         builder.append("<th>screenPercentage</th>");
         builder.append("<th>heapPercentage</th>");           

         Set<Long> keys = sorted.keySet();

         for(Long key : keys) {
            Object uniqueKey = sorted.get(key);
            ProcessResource resource = table.get(uniqueKey);
            String path = resource.getResourceName();
            String name = path.replaceAll(".*(\\/|\\\\)", "");
            String owner = resource.getResourceOwner();
            String thread = resource.getAllocationThread();
            long time = resource.getAllocationTime();
            long difference = time - startTime;
            double size = resource.getSizeEstimate();
            double screenFraction = size / totalSize;
            double heapFraction = size / heapMaxSize;
            long screenPercentage = Math.round(screenFraction * 100.0d);
            long heapPercentage = Math.round(heapFraction * 100.0d);
            String estimate = getSize(size);
            DateTime date = DateTime.at(time);

            builder.append("<tr>");
            builder.append("<td><a href='operationInvoked?component=ResourceMonitor&operation=showResources&signature=1&filter=");
            builder.append(uniqueKey);
            builder.append("'>");
            builder.append(uniqueKey);
            builder.append("</a></td>");
            builder.append("<td>").append(name).append("</td>");
            builder.append("<td>").append(owner).append("</td>");
            builder.append("<td>").append(thread).append("</td>");
            builder.append("<td>").append(date).append("</td>");
            builder.append("<td>").append(difference).append("</td>");    
            builder.append("<td>").append(estimate).append("</td>");
            builder.append("<td>").append(screenPercentage).append("%</td>");  
            builder.append("<td>").append(heapPercentage).append("%</td>");            
            builder.append("</tr>");
            startTime = time;
         }
         builder.append("</table>");
         return builder.toString();
      }
      return null;
   }

   @ManagedOperation("Show the currently allocated resources")
   public String showResources() {
      return showResources(".*");
   }

   @ManagedOperation("Show the currently allocated resources")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="filter", description="Filter resources to show")
   })
   public String showResources(String filter) {
      List<ProcessResource> resources = process.getProcessResources();

      if(resources != null) {
         StringBuilder builder = new StringBuilder();

         builder.append("<table border='1'>");
         builder.append("<th>uniqueKey</th>");
         builder.append("<th>name</th>");
         builder.append("<th>owner</th>");
         builder.append("<th>time</th>");
         builder.append("<th>size</th>");
         builder.append("<th>origin</th>");

         for(ProcessResource resource : resources) {
            Object uniqueKey = resource.getUniqueKey();
            String textKey = String.valueOf(uniqueKey);
            String name = resource.getResourceName();
            String origin = resource.getResourceOrigin();
            String owner = resource.getResourceOwner();

            if(isMatch(textKey, filter) || isMatch(name, filter) || isMatch(owner, filter)) {
               long time = resource.getAllocationTime();
               long size = resource.getSizeEstimate();
               String estimate = getSize(size);
               DateTime date = DateTime.at(time);

               builder.append("<tr>");
               builder.append("<td>").append(uniqueKey).append("</td>");
               builder.append("<td>").append(name).append("</td>");
               builder.append("<td>").append(owner).append("</td>");
               builder.append("<td>").append(date).append("</td>");
               builder.append("<td>").append(estimate).append("</td>");
               builder.append("<td><pre>");
               builder.append(origin);
               builder.append("<pre></td>");
               builder.append("</tr>");
            }
         }
         builder.append("</table>");
         return builder.toString();
      }
      return null;
   }

   private boolean isMatch(String token, String filter) {
      if(token != null && filter != null) {
         return token.equals(filter) || token.matches(filter);
      }
      return token == null && filter == null;
   }

   private String getSize(double size) {
      long rounded = Math.round(size);

      if(rounded < 1024) {
         return rounded + "B";
      }
      if(rounded < 1024 * 1024) {
         return rounded / 1024 + "K";
      }
      if(rounded < 1024 * 1024 * 1024) {
         return rounded / (1024 * 1024) + "M";
      }
      return rounded / (1024 * 1024 * 1024) + "G";
   }
}
