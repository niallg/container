package com.zuooh.container.manager.bridge.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;

public class ContainerBridgeUserComparatorTest extends TestCase {
   
   public void testComparator() throws Exception {
      ContainerBridgeUserComparator comparator = new ContainerBridgeUserComparator();
      ContainerBridgeUser user1 = new ContainerBridgeUser("tom", 5000);
      ContainerBridgeUser user2 = new ContainerBridgeUser("tom", 5000);
      List<ContainerBridgeUser> users = new ArrayList<ContainerBridgeUser>();
      
      user1.setTimeDifference(0);
      
      Thread.sleep(2000);
      
      user2.setTimeDifference(1);
      
      users.add(user1);
      users.add(user2);
      
      Collections.sort(users, comparator);
      
      assertEquals(users.get(0), user2);
      assertEquals(users.get(1), user1);
      
      Thread.sleep(100);
      
      user1.setTimeDifference(0);
      
      Collections.sort(users, comparator);
      
      assertEquals(users.get(0), user1);
      assertEquals(users.get(1), user2);
      
   }

}
