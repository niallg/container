package com.zuooh.container.manager.bridge.message;

import com.zuooh.container.manager.console.ContainerResponseEvent;
import com.zuooh.container.manager.console.ContainerStatusEvent;

public interface ContainerBridgeListener {
   void onStatus(ContainerStatusEvent event);
   void onResponse(ContainerResponseEvent event);
}
