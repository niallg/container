package com.zuooh.container.manager.bridge.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.html.TableDrawer;

@ManagedResource(description="Contains the container users")
public class ContainerBridgeRegistry {

   private final Cache<String, ContainerBridgeUser> registry;
   private final Set<ContainerBridgeUser> users;
   
   public ContainerBridgeRegistry() {
      this.registry = new LeastRecentlyUsedCache<String, ContainerBridgeUser>();
      this.users = new CopyOnWriteArraySet<ContainerBridgeUser>();
   }
   
   @ManagedOperation(description="Show container users")
   public String showUsers() {
      TableDrawer drawer = new TableDrawer("user", "platform", "version", "latency", "updateCount", "timeDifference");
      
      for(ContainerBridgeUser user : users) {
         String name = user.getName();
         String platform = user.getPlatform();
         String version = user.getVersion();
         long latency = user.getUpdateLatency();
         long updateCount = user.getUpdateCount();
         long timeDifference = user.getTimeDifference();
         
         drawer.newRow(name, platform, version, latency, updateCount, timeDifference);
      }
      return drawer.drawTable();
   }
   
   public ContainerBridgeUser get(String name) {
      return registry.fetch(name);
   }
   
   public List<ContainerBridgeUser> listUsers() {
      List<ContainerBridgeUser> list = new ArrayList<ContainerBridgeUser>();
      
      for(ContainerBridgeUser user : users) {
         long time = user.getTimeSinceLastUpdate();
         
         if(time != -1) {
            list.add(user);
         }
      }
      return list;
   }
   
   public List<ContainerBridgeUser> listMostRecentUsers() {
      ContainerBridgeUserComparator comparator = new ContainerBridgeUserComparator();
      List<ContainerBridgeUser> list = new ArrayList<ContainerBridgeUser>();
      
      for(ContainerBridgeUser user : users) {
         long time = user.getTimeSinceLastUpdate();
         
         if(time != -1) {
            list.add(user);
         }
      }
      Collections.sort(list, comparator);
      return list;
   }
   
   public void register(ContainerBridgeUser user) {
      String name = user.getName();      
      ContainerBridgeUser previous = registry.take(name);
      
      if(previous != null) {
         users.remove(previous);
      }
      registry.cache(name, user);
      users.add(user);
   }
}
