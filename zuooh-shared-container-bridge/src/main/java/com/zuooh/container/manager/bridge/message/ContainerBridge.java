package com.zuooh.container.manager.bridge.message;

import java.io.DataInputStream;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.io.Base64InputStream;
import com.zuooh.container.manager.console.ContainerResponseEvent;
import com.zuooh.container.manager.console.ContainerStatusEvent;

public class ContainerBridge implements ContainerBridgeListener {
   
   private static final Logger LOG = LoggerFactory.getLogger(ContainerBridge.class);
   
   private final ContainerBridgeRegistry registry;
   
   public ContainerBridge(ContainerBridgeRegistry registry) {
      this.registry = registry;
   }

   @Override
   public void onStatus(ContainerStatusEvent event) {
      long announceTime = event.getAnnounceTime();
      long currentTime = System.currentTimeMillis();
      long timeDifference = announceTime - currentTime;
      String name = event.getUser();
      String platform = event.getPlatform();
      String version = event.getVersion();      
      ContainerBridgeUser user = registry.get(name);
      
      if(user == null) {
         user = new ContainerBridgeUser(name, 10000);
         registry.register(user);
      }
      user.setTimeDifference(timeDifference);
      user.setPlatform(platform);
      user.setVersion(version);
   }

   @Override
   public void onResponse(ContainerResponseEvent event) {
      String name = event.getUser();
      String requestId = event.getRequestId();
      String response = event.getResponse();      
      ContainerBridgeUser user = registry.get(name);
      
      try {
         Base64InputStream decoder = new Base64InputStream(response);
         GZIPInputStream decompressor = new GZIPInputStream(decoder);
         DataInputStream reader = new DataInputStream(decompressor);
         
         if(user == null) {
            user = new ContainerBridgeUser(name, 10000);
            registry.register(user);
         }         
         String result = reader.readUTF();
         
         user.setResponse(requestId, result);
         reader.close();
      } catch(Exception e) {
         LOG.info("Coult not decode the response", e);
      }
   }

}
