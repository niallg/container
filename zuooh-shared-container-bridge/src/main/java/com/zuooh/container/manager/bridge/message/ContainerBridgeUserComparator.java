package com.zuooh.container.manager.bridge.message;

import java.util.Comparator;

public class ContainerBridgeUserComparator implements Comparator<ContainerBridgeUser> {

   @Override
   public int compare(ContainerBridgeUser left, ContainerBridgeUser right) {
      Long leftTime = left.getTimeSinceLastUpdate();
      Long rightTime = right.getTimeSinceLastUpdate();
      
      return leftTime.compareTo(rightTime);      
   }

}
