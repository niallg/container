package com.zuooh.container.manager.bridge.message;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.container.manager.console.ContainerResponseEvent;
import com.zuooh.container.manager.console.ContainerStatusEvent;
import com.zuooh.message.Message;
import com.zuooh.message.MessageListener;

public class ContainerBridgeMessageListener implements MessageListener {

   private static final Logger LOG = LoggerFactory.getLogger(ContainerBridgeMessageListener.class);
   
   private final ContainerBridgeListener listener;
   
   public ContainerBridgeMessageListener(ContainerBridgeListener listener) {  
      this.listener = listener;
   }  

   @Override
   public void onMessage(Message message) {
      Object value = message.getValue();
      
      if(value instanceof ContainerStatusEvent) {
         listener.onStatus((ContainerStatusEvent)value);
      }
      if(value instanceof ContainerResponseEvent) {
         listener.onResponse((ContainerResponseEvent)value);
      }           
   }

   @Override
   public void onException(Exception cause) {
      LOG.info("Got exception", cause);
   }

   @Override
   public void onHeartbeat() {
      LOG.info("Got heartbeat");
   }

   @Override
   public void onReset() {
      LOG.info("Got reset"); 
   }
}
