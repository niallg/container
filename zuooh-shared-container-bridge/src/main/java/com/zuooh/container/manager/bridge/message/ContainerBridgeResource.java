package com.zuooh.container.manager.bridge.message;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.simpleframework.http.Path;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;

import com.zuooh.container.manager.console.ContainerRequestEvent;
import com.zuooh.http.resource.Resource;
import com.zuooh.message.Message;
import com.zuooh.message.MessagePublisher;

public class ContainerBridgeResource implements Resource {
   
   private final ContainerBridgeRegistry registry;
   private final MessagePublisher publisher;
   private final String prefix;
   
   public ContainerBridgeResource(ContainerBridgeRegistry registry, MessagePublisher publisher, String prefix) {
      this.publisher = publisher;
      this.registry = registry;
      this.prefix = prefix;
   }

   @Override
   public void handle(Request request, Response resp) throws Throwable {
      String requestId = System.currentTimeMillis() + "-request";
      Path path = request.getPath();
      String normal = path.getPath();
      int prefixLength = prefix.length();
      String pathWithoutPrefix = normal.substring(prefixLength);
      Map<String, String> query = request.getQuery();
      Pattern pattern = Pattern.compile("/(.*)(/manage.*)");
      Matcher matcher = pattern.matcher(pathWithoutPrefix);
      
      if(!matcher.matches()) {
         throw new IllegalStateException("Path '" + pathWithoutPrefix + "' did not match required pattern");
      }
      String clientKey = matcher.group(1);
      String requestPath = matcher.group(2);
      
      Map<String, String> attributes = new HashMap<String, String>(query);
      ContainerRequestEvent requestEvent = new ContainerRequestEvent(clientKey, requestPath, requestId, attributes);
      Message message = new Message(requestEvent);
      
      publisher.publish(message);
      
      PrintStream stream = resp.getPrintStream();
      ContainerBridgeUser user = registry.get(clientKey);
      String result = user.getResponse(requestId); // here we block until there is a response message!!!

      if(result == null) {
         throw new IllegalStateException("No result for '" + requestId + "'");
      }
      resp.setStatus(Status.OK);
      resp.setContentType("text/html");
      stream.print(result);
      stream.close();
   }
}
