package com.zuooh.container.manager.bridge.message;

import static org.simpleframework.http.Protocol.CACHE_CONTROL;
import static org.simpleframework.http.Protocol.NO_CACHE;

import java.util.List;
import java.util.Map;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;

import com.zuooh.http.resource.Resource;

public class ContainerBridgeListResource implements Resource {

   private final ContainerBridgeRegistry registry;
   private final Resource resource;
   private final String attribute;
   
   public ContainerBridgeListResource(ContainerBridgeRegistry registry, Resource resource, String attribute) {
      this.resource = resource;
      this.registry = registry;
      this.attribute = attribute;
   }               
   
   public void handle(Request request, Response response) throws Throwable {     
      List<ContainerBridgeUser> users = registry.listMostRecentUsers();
      Map attributes = request.getAttributes();
      
      response.setValue(CACHE_CONTROL, NO_CACHE);
      attributes.put(attribute, users);    
      resource.handle(request, response);
   } 
}
