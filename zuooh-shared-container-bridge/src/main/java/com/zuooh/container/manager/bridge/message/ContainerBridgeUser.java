package com.zuooh.container.manager.bridge.message;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.time.DateTime;

public class ContainerBridgeUser {   
   
   private static final Logger LOG = LoggerFactory.getLogger(ContainerBridgeUser.class);
   
   private final AtomicReference<String> platform;
   private final AtomicReference<String> version;
   private final UserMessageExchanger queue;
   private final AtomicLong timeDifference;
   private final AtomicLong updateLatency;
   private final AtomicLong updateCount;
   private final AtomicLong updateTime;
   private final String name;

   public ContainerBridgeUser(String name, int wait) {
      this.version = new AtomicReference<String>();
      this.platform = new AtomicReference<String>();
      this.queue = new UserMessageExchanger(wait);
      this.timeDifference = new AtomicLong();
      this.updateLatency = new AtomicLong();
      this.updateTime = new AtomicLong();
      this.updateCount = new AtomicLong();
      this.name = name;
   }
   
   public String getName() {
      return name;
   }
   
   public String getPlatform() {
      return platform.get();
   }
   
   public void setPlatform(String platform) {
      this.platform.set(platform);
   }
   
   public String getVersion() {
      return version.get();
   }
   
   public void setVersion(String version) {
      this.version.set(version);
   }

   public long getUpdateCount() {
      return updateCount.get();
   }
   
   public long getUpdateLatency() {
      return updateLatency.get();
   }
   
   public String getUpdateTime() {
      long time = updateTime.get();
      DateTime date = DateTime.at(time);
      
      return date.toString();
   }   
   
   public long getTimeSinceLastUpdate() {
      long currentTime = System.currentTimeMillis();
      long lastTime = updateTime.get();

      if(lastTime > 0) {
         return currentTime - lastTime;
      }
      return -1;
   }
   
   public long getTimeDifference() {
      return timeDifference.get();
   }
   
   public void setTimeDifference(long difference) {
      long currentTime = System.currentTimeMillis();
      
      updateTime.set(currentTime);
      updateCount.getAndIncrement();
      timeDifference.set(difference);
      updateCount.getAndIncrement();
   }  
   
   public String getResponse(String requestId) {
      return queue.getResponse(requestId);
   }
   
   public void setResponse(String requestId, String response) {
      queue.setResponse(requestId, response);
   }
   
   private class UserMessageExchanger {
      
      private final Map<String, BlockingQueue<String>> listeners;
      private final int wait;
      
      public UserMessageExchanger(int wait) {
         this.listeners = new ConcurrentHashMap<String, BlockingQueue<String>>();
         this.wait = wait;
      }
      
      public String getResponse(String requestId) {
         BlockingQueue<String> queue = listeners.get(requestId);
         
         try {
            try {            
               if(queue == null) {
                  queue = new LinkedBlockingQueue<String>();        
                  listeners.put(requestId, queue);
               }
               long start = System.currentTimeMillis();
               String response = queue.poll(wait, TimeUnit.MILLISECONDS);
               
               if(response != null) {
                  long finish = System.currentTimeMillis();
                  long duration = finish - start;
                  
                  updateTime.set(finish);
                  updateCount.getAndIncrement();
                  updateLatency.set(duration);
                  
                  return response;
               }
            } catch(Exception e) {
               LOG.info("Could not poll for response for " + requestId);
            }
         } finally {
            listeners.remove(requestId);
         }
         return null;
      }

      public void setResponse(String requestId, String response) {
         BlockingQueue<String> queue = listeners.get(requestId);
         
         if(queue == null) {
            queue = new LinkedBlockingQueue<String>();        
            listeners.put(requestId, queue);
         }
         queue.offer(response);
      }
   }
}
