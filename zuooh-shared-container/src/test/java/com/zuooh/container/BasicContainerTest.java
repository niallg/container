package com.zuooh.container;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import com.zuooh.container.BasicContainer;

import junit.framework.TestCase;

public class BasicContainerTest extends TestCase {

   public static class ConcurrentContainer extends BasicContainer {

      public ConcurrentContainer() {
         super();
      }

      public void register(String name, Object value) {
         System.err.println("REGISTER: " + name);
         super.register(name, value);
      }

      @Override
      public void loadAll() {
         for(int i = 0; i < 100; i++) {
            try {
               Thread.sleep(100);
            } catch(Exception e) {
               e.printStackTrace();
            }
            register(String.format("key-%s", i), String.format("value-%s", i));
         }
      }
   }

   public void testContainer() throws Exception {
      final CountDownLatch latch = new CountDownLatch(5);
      final ConcurrentContainer container = new ConcurrentContainer();
      final BlockingQueue<String> queue = new LinkedBlockingQueue<String>();

      new Thread(new Runnable() {

         @Override
         public void run() {
            latch.countDown();

            try {
               latch.await();
               container.loadAll();
            } catch(Exception e) {
               e.printStackTrace();
            }
         }
      }).start();

      for(int i = 0; i < 5; i++) {
         new Thread(new Runnable() {

            @Override
            public void run() {
               latch.countDown();

               try {
                  latch.await();
                  long start = System.currentTimeMillis();
                  String value = container.lookup("key-86", 20000);
                  long duration = System.currentTimeMillis() - start;
                  System.err.println("Found " + value + " after " + duration + "ms");
                  queue.offer(value);
               } catch(Exception e) {
                  e.printStackTrace();
               }
            }
         }).start();
      }

      for(int i = 0; i < 5; i++) {
         String value = queue.take();
         assertEquals(value, "value-86");
      }
   }
}
