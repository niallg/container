package com.zuooh.container;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BasicContainer implements Container {
   
   private static final Logger LOG = LoggerFactory.getLogger(BasicContainer.class);

   private final CopyOnWriteArraySet<Runnable> stopFunctions;
   private final Map<String, Object> registryByName;
   private final Map<Class, Object> registryByClass;
   private final Set<Condition> registerConditions;
   private final Set<Class> excludedTypes;
   private final Lock registerLock;

   public BasicContainer() {
      this.registerConditions = new CopyOnWriteArraySet<Condition>();
      this.registryByName = new ConcurrentHashMap<String, Object>();
      this.registryByClass = new ConcurrentHashMap<Class, Object>();
      this.excludedTypes = new CopyOnWriteArraySet<Class>();
      this.stopFunctions = new CopyOnWriteArraySet<Runnable>();
      this.registerLock = new ReentrantLock(true);
   }

   @Override
   public void register(String name, Object value) {
      registerLock.lock();

      try {
         for(Condition condition : registerConditions) {
            condition.signalAll();
         }
         registerByType(value);
         registerByName(name,  value);
      } finally {
         registerLock.unlock();
      }

   }

   public void registerStopFunction(Runnable stopFunction) {
      stopFunctions.add(stopFunction);
   }

   private void registerByName(String name, Object value) {
      registryByName.put(name,  value);
   }

   private void registerByType(Object value) {
      Class type = value.getClass();
      Object existing = registryByClass.get(type);

      if(existing != null) {
         if(existing != value) {
            registryByClass.remove(type);
            excludedTypes.add(type);
         }
      } else {
         if(!excludedTypes.contains(type)) {
            registryByClass.put(type,  value);
         }
      }
   }

   @Override
   public void loadFrom(String configFile) {
      LOG.info("Loading from a configuration file is not supported");
   }

   @Override
   public void startAll() {
      LOG.info("Start all has not been implemented");
   }

   @Override
   public void stopAll() {
      for(Runnable stopFunction : stopFunctions) {
         try {
            stopFunction.run();
         } catch(Throwable e) {
            LOG.info("Could not run stop function", e);
         }
      }
   }

   @Override
   public boolean contains(String name) {
      return registryByName.containsKey(name);
   }

   public boolean contains(Class type) {
      Object value = registryByClass.get(type);

      if(value == null) {
         Set<String> names = registryByName.keySet();

         for(String name : names) {
            Object result = registryByName.get(name);
            Class actual = result.getClass();

            if(type.isAssignableFrom(actual)) {
               registryByClass.put(type, result);
               return true;
            }
         }
         return false;
      }
      return true;
   }

   @Override
   public Set<String> listAll() {
      return Collections.unmodifiableSet(registryByName.keySet());
   }

   @Override
   public Set<String> listMatches(Class<? extends Annotation> annotation) {
      Set<String> names = registryByName.keySet();

      if(!names.isEmpty()) {
         Set<String> matches = new TreeSet<String>();

         for(String name : names) {
            Object result = registryByName.get(name);
            Class actual = result.getClass();
            Annotation value = actual.getAnnotation(annotation);

            if(value != null) {;
               matches.add(name);
            }
         }
         return matches;
      }
      return Collections.emptySet();
   }
   
   @Override
   public <T> Set<T> lookupAll(Class<T> type) {
      Set<String> names = registryByName.keySet();

      if(!names.isEmpty()) {
         Set<T> matches = new HashSet<T>();

         for(String name : names) {
            Object result = registryByName.get(name);
            Class actual = result.getClass();
            
            if(type.isAssignableFrom(actual)) {
               matches.add((T)result);
            }
         }
         return matches;
      }
      return Collections.emptySet();
   }

   @Override
   public <T> T lookup(Class<T> type) {
      Object value = registryByClass.get(type);

      if(value == null) {
         Set<String> names = registryByName.keySet();

         for(String name : names) {
            Object result = registryByName.get(name);
            Class actual = result.getClass();

            if(type.isAssignableFrom(actual)) {
               registryByClass.put(type, result);
               return (T)result;
            }
         }
      }
      return (T)value;
   }

   public <T> T lookup(String name, long wait) {
      long start = System.currentTimeMillis();
      long expiry = start + wait;

      while(!contains(name)) {
         long duration = expiry - System.currentTimeMillis();

         if(duration <= 0) {
            throw new IllegalStateException("Could not find '" + name + "' within " + wait + " milliseconds");
         }
         Condition condition = registerLock.newCondition();

         registerLock.lock();
         registerConditions.add(condition);

         try {
            if(contains(name)) {
               return (T)registryByName.get(name);
            }
            condition.await(duration, MILLISECONDS);
         } catch(Exception e) {
            LOG.info("Could not lookup " + name, e);
         } finally {
           registerConditions.remove(condition);
           registerLock.unlock();
         }
      }
      return (T)registryByName.get(name);
   }

   public <T> T lookup(Class type, long wait) {
      long start = System.currentTimeMillis();
      long expiry = start + wait;

      while(!contains(type)) {
         long duration = expiry - System.currentTimeMillis();

         if(duration <= 0) {
            throw new IllegalStateException("Could not find " + type + " within " + wait + " milliseconds");
         }
         Condition condition = registerLock.newCondition();

         registerLock.lock();
         registerConditions.add(condition);

         try {
            if(contains(type)) {
               return (T)registryByClass.get(type);
            }
            condition.await(duration, MILLISECONDS);
         } catch(Exception e) {
            LOG.info("Could not lookup " + type, e);
         } finally {
           registerConditions.remove(condition);
           registerLock.unlock();
         }
      }
      return (T)registryByClass.get(type);
   }

   @Override
   public <T> T lookup(String name) {
      return (T)registryByName.get(name);
   }

   @Override
   public ContainerTracer getTracer() {
      return null;
   }
}
