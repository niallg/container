package com.zuooh.container;

import java.lang.annotation.Annotation;
import java.util.Set;

public interface Container {
   void startAll();
   void stopAll();
   void loadAll();
   void loadFrom(String configFile);
   void register(String name, Object value);
   boolean contains(String name);
   <T> T lookup(Class<T> type);
   <T> Set<T> lookupAll(Class<T> type);
   <T> T lookup(String name);
   Set<String> listAll();
   Set<String> listMatches(Class<? extends Annotation> annotation);
   ContainerTracer getTracer();
}
