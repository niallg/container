package com.zuooh.container;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

public interface ContainerTracer {
   void addParameter(String name, Class type);
   void addConstructorParameter(String name, Class type);
   void newInstanceCreated(String id, Constructor factory, List<String> variables);
   void newPropertiesCreated(String id, String extendId);   
   void newMapCreated(String id, String extendId, Class[] dependents);
   void newListCreated(String id, String extendId, Class[] dependents);
   void newSetCreated(String id, String extendId, Class[] dependents);
   void callMethod(String id, String assignToId, Method method, List<String> variables);
   void addEntryToMap(String id, String key, String variable);
   void addEntryToProperties(String id, String key, String variable);   
   void addEntryToCollection(String id, String variable);
   void addInclude(String fileName);
   void registerComponent(String name, String id);
   void registerStopMethod(String id, Method method, List<String> variables);
}
