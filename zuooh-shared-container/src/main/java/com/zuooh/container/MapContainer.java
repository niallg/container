package com.zuooh.container;

import java.util.Map;
import java.util.Set;

public class MapContainer extends BasicContainer {

   private final Map<String, Object> objects;

   public MapContainer(Map<String, Object> objects) {
      this.objects = objects;
   }

   @Override
   public void loadAll() {
      Set<String> names = objects.keySet();

      for(String name : names) {
         Object value = objects.get(name);

         if(value != null) {
            register(name, value);
         }
      }
   }
}
