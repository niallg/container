package com.zuooh.container.build.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Module extends ModuleContainer implements org.simpleframework.xml.util.Entry {

   @Attribute
   private String name;

   public Module() {
      super();
   }

   @Override
   public String getName() {
      return name;
   }

   @Override
   public String toString() {
      return String.format("module %s", name);
   }
}
