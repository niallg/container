package com.zuooh.container.build.schema;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.container.ContainerTracer;

@Root(name="set")
public class SetComponent implements CollectionComponent{

   @Attribute(required=false)
   private String id;

   @Attribute(required=false)
   private String scope;

   @Attribute(required=false)
   private String parent;

   @ElementListUnion({
      @ElementList(entry="value", type=CollectionValue.class, inline=true, required=false),
      @ElementList(entry="ref", type=CollectionReference.class, inline=true, required=false),
      @ElementList(entry="bean", type=CollectionBean.class, inline=true, required=false)
   })
   private ArrayList<CollectionElement> set;

   @Attribute(name="set-class", required=false)
   private Class use;

   @Attribute(name="value-type", required=false)
   private Class value;
   
   @Transient
   private Set<Object> cache;

   public SetComponent() {
      super();
   }

   @Override
   public String getVariableName() {
      if(id != null) {
         return PropertyMapper.getPropertyName(id) + System.identityHashCode(this);
      }
      return String.format("set" + System.identityHashCode(this));
   }

   @Override
   public String getResultVariableName() {
      return getVariableName();
   }

   @Override
   public Set<Object> getValue(ContainerContext container) throws Exception {
      Class[] type = new Class[]{String.class};

      if(value != null) {
         type[0] = value;
      }
      return getValue(container, type);
   }

   @Override
   public Set<Object> getValue(ContainerContext container, Class[] dependents) throws Exception {
      if(cache == null) {
         Set<Object> value = new LinkedHashSet<Object>();
      
         if(use != null) {
            if(Set.class.isAssignableFrom(use)) {
               throw new IllegalStateException("The set-class " + use + " does not implement java.util.Set");
            }
            value = (Set<Object>)use.newInstance();
         }
         ContainerTracer tracer = container.lookupTracer();
         String variableName = getVariableName();
   
         if(parent != null) {
            Set<Object> set = container.lookupSet(parent, dependents);
            ScopedComponent component = container.componentFor(parent);
            String extendId = component.getVariableName();
   
            value.addAll(set);
            tracer.newSetCreated(variableName, extendId, dependents);
         } else {
            tracer.newSetCreated(variableName, null, dependents);
         }
         if(set != null) {
            for(CollectionElement entry : set) {
               String entryVariableName = entry.getVariableName(container, dependents[0]);
      
               value.add(entry.getValue(container, dependents[0]));
               tracer.addEntryToCollection(variableName, entryVariableName);
            }
         }
         cache = value;
      }
      return cache;
   }

   @Override
   public Scope getScope() {
      return Scope.resolveScope(scope);
   }

   @Override
   public String getName() {
      return id;
   }
}
