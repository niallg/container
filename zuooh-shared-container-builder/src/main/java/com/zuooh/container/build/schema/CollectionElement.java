package com.zuooh.container.build.schema;

public interface CollectionElement {   
   Object getValue(ContainerContext container, Class type) throws Exception;
   String getVariableName(ContainerContext container, Class type) throws Exception;
}
