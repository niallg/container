package com.zuooh.container.build.schema;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.container.ContainerTracer;

@Root(name="props")
public class PropertiesComponent implements CollectionComponent {

   @Attribute(required=false)
   private String id;

   @Attribute(required=false)
   private String scope;

   @Attribute(required=false)
   private String extend;

   @ElementMap(entry="prop", key="key", attribute=true, inline=true, required=false)
   private Map<String, String> properties;
   
   @Transient
   private Properties cache;

   @Override
   public String getVariableName() {
      if(id != null) {
         return PropertyMapper.getPropertyName(id) + System.identityHashCode(this);
      }
      return String.format("props" + System.identityHashCode(this));
   }

   @Override
   public String getResultVariableName() {
      return getVariableName();
   }

   @Override
   public Properties getValue(ContainerContext container) throws Exception {
      Class[] types = new Class[]{String.class, String.class};
      return getValue(container, types);
   }

   @Override
   public Properties getValue(ContainerContext container, Class[] dependents) throws Exception {
      if(cache == null) {
         Properties result = new Properties();
         ContainerTracer tracer = container.lookupTracer();
         String variableName = getVariableName();
   
         if(extend != null) {
            Properties found = container.lookupProperties(extend);
            ScopedComponent component = container.componentFor(extend);
            String extendId = component.getVariableName();
   
            result.putAll(found);
            tracer.newPropertiesCreated(variableName, extendId);
         } else {
            tracer.newPropertiesCreated(variableName, null);
         }
         if(properties != null) {
            Set<String> names = properties.keySet();
         
            for(String name : names) {
               String value = properties.get(name);
               
               result.setProperty(name, value);
               tracer.addEntryToProperties(variableName, name, value);
            }
         }
         cache = result;
      }
      return cache;
   }

   @Override
   public Scope getScope() {
      return Scope.resolveScope(scope);
   }

   @Override
   public String getName() {
      return id;
   }
}
