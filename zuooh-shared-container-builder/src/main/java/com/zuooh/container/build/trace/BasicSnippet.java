package com.zuooh.container.build.trace;

import java.util.Map;

public abstract class BasicSnippet implements ContainerSnippet {

   @Override
   public void assignments(Map<String, Class> assignments) throws Exception {}
}
