package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class AddInclude extends BasicSnippet {

   private final String fileName;

   public AddInclude(String fileName) {
      this.fileName = fileName;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      sourceFiles.add(fileName);
   }
}
