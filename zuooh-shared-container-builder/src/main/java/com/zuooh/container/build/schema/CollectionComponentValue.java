package com.zuooh.container.build.schema;

public class CollectionComponentValue implements CollectionComponent {
   
   private final CollectionComponent component;
   private final Object value;
   
   public CollectionComponentValue(CollectionComponent component, Object value) {
      this.component = component;
      this.value = value;
   }

   @Override
   public String getResultVariableName() {
      return component.getResultVariableName();
   }

   @Override
   public String getVariableName() {
      return component.getVariableName();
   }

   @Override
   public Scope getScope() {
      return component.getScope();
   }

   @Override
   public String getName() {
      return component.getName();
   }

   @Override
   public Object getValue(ContainerContext container) throws Exception {
      if(value == null) {
         return component.getValue(container);
      }
      return value;
      
   }

   @Override
   public Object getValue(ContainerContext container, Class[] dependents) throws Exception {
      if(value == null) {
         return component.getValue(container, dependents);
      }
      return value;
   }

}
