package com.zuooh.container.build.schema;

import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.Transient;
import org.simpleframework.xml.core.Commit;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.core.Validate;
import org.simpleframework.xml.util.Dictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.FileSystem;
import com.zuooh.common.function.FunctionParameter;
import com.zuooh.container.Container;
import com.zuooh.container.ContainerTracer;
import com.zuooh.container.build.trace.ContainerGenerator;

@Root(name="beans")
public class ModuleContainer implements Container {  
   
   private static final Logger LOG = LoggerFactory.getLogger(ModuleContainer.class);
   
   @ElementList(inline=true, required=false)
   private Dictionary<PropertiesComponent> properties;    

   @ElementList(inline=true, required=false)
   private Dictionary<Component> beans;

   @ElementList(inline=true, required=false)
   private Dictionary<MapComponent> maps;  

   @ElementList(inline=true, required=false)
   private Dictionary<ListComponent> lists;

   @ElementList(inline=true, required=false)
   private Dictionary<SetComponent> sets;

   @ElementList(inline=true, required=false)
   private Dictionary<Import> imports;

   @ElementList(inline=true, required=false)
   private Dictionary<Module> modules;
   
   @ElementList(inline=true, required=false)
   private Dictionary<Variable> variables;
   
   @Attribute(required=false)
   private String schemaLocation;

   @Attribute(required=false)
   private String[] priority;  

   @Transient
   private Map<String, Object> registryByName;

   @Transient
   private Map<Class, Object> registryByClass;

   @Transient
   private Map<String, Object> registryByFactory;

   @Transient
   private Set<Class> excludedTypes;

   @Transient
   private Set<Component> started;

   @Transient
   private ModuleContainer parent;

   @Transient
   private ModuleContext delegate;

   @Transient
   private FunctionExtractor extractor;

   @Transient
   private ContainerTracer tracer;
   
   @Transient
   private String source;

   public ModuleContainer() {
      this.registryByName = new ConcurrentHashMap<String, Object>();
      this.registryByClass = new ConcurrentHashMap<Class, Object>();
      this.registryByFactory = new ConcurrentHashMap<String, Object>();
      this.excludedTypes = new CopyOnWriteArraySet<Class>();
      this.properties = new Dictionary<PropertiesComponent>();
      this.beans = new Dictionary<Component>();
      this.modules = new Dictionary<Module>();
      this.maps = new Dictionary<MapComponent>();
      this.sets = new Dictionary<SetComponent>();
      this.lists = new Dictionary<ListComponent>();
      this.extractor = new FunctionExtractor(this);
      this.delegate = new ModuleContext(this, extractor);
      this.started = new HashSet<Component>();
      this.tracer = new ContainerGenerator();
   }

   @Validate
   private void validate() {
      validateComponents();
      validateCollections();
   }

   @Commit
   private void commit() {
      registerModules();
   }

   private void validateComponents() {
      for(Component component : beans) {
         String name = component.getName();

         if(maps.contains(name)) {
            throw new IllegalStateException("Map '" + name + "' has the same id as an existing component");
         }
         if(sets.contains(name)) {
            throw new IllegalStateException("Set '" + name + "' has the same id as an existing component");
         }
         if(lists.contains(name)) {
            throw new IllegalStateException("List '" + name + "' has the same id as an existing component");
         }
      }
   }

   private void validateCollections() {
      Persister persister = new Persister();
      StringWriter writer = new StringWriter();

      for(MapComponent component : maps){
         String name = component.getName();

         if(name == null) {
            try {
               persister.write(component, writer);
               throw new IllegalStateException("Map declared with no id:\n" + writer);
            } catch(Exception e) {
               throw new IllegalStateException("Map declared with no id", e);
            }
         }
      }
      for(SetComponent component : sets){
         String name = component.getName();

         if(name == null) {
            try {
               persister.write(component, writer);
               throw new IllegalStateException("Set declared with no id:\n" + writer);
            } catch(Exception e) {
               throw new IllegalStateException("Set declared with no id", e);
            }
         }
      }
      for(ListComponent component : lists){
         String name = component.getName();

         if(name == null) {
            try {
               persister.write(component, writer);
               throw new IllegalStateException("List declared with no id:\n" + writer);
            } catch(Exception e) {
               throw new IllegalStateException("List declared with no id", e);
            }
         }
      }
   }

   private void registerModules() {
      for(ModuleContainer module : modules) {
         module.parent = this;
         module.extractor = extractor;
      }
   }

   @Override
   public void loadFrom(String configFile) {
      LOG.info("Cannot load from " + configFile + " as operation not supported");
   }

   @Override
   public void startAll() {
      if(beans != null) {
         try {
            Set<String> dependencyOrder = delegate.lookupOrder();

            for(String componentName : dependencyOrder) {
               Component component = beans.get(componentName);
               start(component);
            }
            for(Component component : beans) {
               start(component);
            }
         } catch(Exception e) {
            throw new IllegalStateException("Could not start all components", e);
         }
      }
      if(modules != null) {
         for(Module module : modules) {
            module.startAll();
         }
      }
   }

   private void start(Component component) {
      if(component != null && !started.contains(component)) {
         String name = component.getName();

         try {
            FunctionCall startMethod = component.getStartMethod(delegate);

            if(startMethod != null) {
               Object value = find(name);

               if(!started.contains(component)) {
                  try {
                     String variableName = component.getVariableName();
                     startMethod.call(variableName, null, delegate, value);
                     started.add(component);
                  } catch(Exception e) {
                     throw new IllegalStateException("Error starting " + name, e);
                  }
               }
            }
         } catch(Exception e) {
            throw new IllegalStateException("Could not start component " + name, e);
         }
      }
   }

   @Override
   public void stopAll() {
      if(beans != null) {
         try {
            for(Component component : beans) {
               String name = component.getName();
               FunctionCall stopMethod = component.getStopMethod(delegate);

               if(stopMethod != null) {
                  Object value = find(name);
                  String variableName = component.getVariableName();

                  try {
                     stopMethod.call(variableName, null, delegate, value);
                  } catch(Exception e) {
                     throw new IllegalStateException("Error calling " + stopMethod + " with " + name);
                  }
               }
            }
         } catch(Exception e) {
            throw new IllegalStateException("Could not stop all components", e);
         }
      }
      if(modules != null) {
         for(Module module : modules) {
            module.stopAll();
         }
      }
   }

   @Override
   public void loadAll() {
      try {
         if(priority != null) {
            for(String name : priority) {
               lookup(name);
            }
         }
         if(beans != null) {
            for(Component component : beans) {
               String name = component.getName();
   
               if(name != null) {
                  lookup(name);
               }
            }
         }
         if(modules != null) {
            for(Module module : modules) {
               module.loadAll();
            }
         }
      } catch(Exception e) {
         throw new IllegalStateException("Error loading from " + source, e);
      }
   }
   
   public void registerSource(String source) {
      this.source = source;
   }

   public void importAll(FileSystem environment, Serializer serializer) throws Exception {
      if(imports != null) {
         for(Import resource : imports) {
            ModuleContainer container = resource.getContainer(environment, serializer);
            String fileName = resource.getName();
            
            container.registerParent(this);
            container.importAll(environment, serializer);
            
            if(source != null) {
               container.registerSource(source + "/" + fileName);
            } else {
               container.registerSource(fileName);
            }
            ContainerTracer tracer = getTracer();
            
            container.registerParent(this);
            tracer.addInclude(fileName);
            importComponents(container, resource);
            importMaps(container, resource);
            importSets(container, resource);
            importLists(container, resource);
            importModules(container, resource);
         }
      }
   }

   private void importModules(ModuleContainer container, Object resource) {
      for(Module component : container.modules) {
         String id = component.getName();
         Module existing = modules.get(id);

         if(existing != null) {
            throw new IllegalStateException("Duplicate module '" +id+ "' declared in '"+ resource +"'");
         }
         modules.add(component);
      }
   }

   private void importComponents(ModuleContainer container, Object resource) {
      for(Component component : container.beans) {
         String id = component.getName();
         Component existing = beans.get(id);

         if(existing != null) {
            throw new IllegalStateException("Duplicate component '" +id+ "' declared in '"+ resource +"'");
         }
         beans.add(component);
      }
   }

   private void importMaps(ModuleContainer container, Object resource) {
      for(MapComponent component : container.maps) {
         String id = component.getName();
         Component existingComponent = beans.get(id);
         MapComponent existingMap = maps.get(id);

         if(existingComponent != null) {
            throw new IllegalStateException("Map '" +id+ "' declared in '"+ resource +"' conflicts with component of same name");
         }
         if(existingMap != null) {
            throw new IllegalStateException("Map '" +id+ "' declared in '"+ resource +"' conflicts with map of same name");
         }
         maps.add(component);
      }
   }

   private void importSets(ModuleContainer container, Object resource) {
      for(SetComponent component : container.sets) {
         String id = component.getName();
         Component existingComponent = beans.get(id);
         SetComponent existingSet = sets.get(id);

         if(existingComponent != null) {
            throw new IllegalStateException("Set '" +id+ "' declared in '"+ resource +"' conflicts with component of same name");
         }
         if(existingSet != null) {
            throw new IllegalStateException("Set '" +id+ "' declared in '"+ resource +"' conflicts with set of same name");
         }
         sets.add(component);
      }
   }

   private void importLists(ModuleContainer container, Object resource) {
      for(ListComponent component : container.lists) {
         String id = component.getName();
         Component existingComponent = beans.get(id);
         ListComponent existingList = lists.get(id);

         if(existingComponent != null) {
            throw new IllegalStateException("List '" +id+ "' declared in '"+ resource +"' conflicts with component of same name");
         }
         if(existingList != null) {
            throw new IllegalStateException("List '" +id+ "' declared in '"+ resource +"' conflicts with map of same name");
         }
         lists.add(component);
      }
   }

   @Override
   public void register(String name, Object value) {
      registerByType(value);
      registerByName(name,  value);
   }

   private void registerByName(String name, Object value) {
      registryByName.put(name,  value);
   }

   private void registerByFactory(String name, Object value) {
      registryByFactory.put(name,  value);
   }

   private void registerByType(Object value) {
      Class type = value.getClass();
      Object existing = registryByClass.get(type);

      if(existing != null) {
         if(existing != value) {
            registryByClass.remove(type);
            excludedTypes.add(type);
         }
      } else {
         if(!excludedTypes.contains(type)) {
            registryByClass.put(type,  value);
         }
      }
   }

   private void registerParent(ModuleContainer parent) {    
      this.parent = parent;
   }

   @Override
   public boolean contains(String name) {
      if(!beans.contains(name) && !registryByName.containsKey(name)) {
         for(Module module : modules) {
            if(module.contains(name)) {
               return true;
            }
         }
      }
      return true;
   }

   @Override
   public Set<String> listAll() {
      Set<String> matches = new TreeSet<String>();

      for(Component component : beans) {
         String name = component.getName();
         matches.add(name);
      }
      for(Module module : modules) {
         Set<String> additional = module.listAll();
         matches.addAll(additional);
      }
      return matches;
   }

   @Override
   public Set<String> listMatches(Class<? extends Annotation> annotation) {
      Set<String> matches = new TreeSet<String>();

      for(Component component : beans) {
         Class<?> componentType = component.getType();
         
         if(componentType != null) {
            Annotation value = componentType.getAnnotation(annotation);
   
            if(value != null) {
               String name = component.getName();
               matches.add(name);
            }
         }
      }
      for(Module module : modules) {
         Set<String> additional = module.listMatches(annotation);
         matches.addAll(additional);
      }
      Set<String> registeredNames = registryByName.keySet();

      for(String registeredName : registeredNames) {
         if(!matches.contains(registeredName)) {
            Object object = registryByName.get(registeredName);
            Class<?> type = object.getClass();
            Annotation value = type.getAnnotation(annotation);

            if(value != null) {
               matches.add(registeredName);
            }
         }
      }
      return matches;
   }
   
   @Override
   public <T> Set<T> lookupAll(Class<T> type) {
      Set<T> matches = new HashSet<T>();

      for(Component component : beans) {
         Class<?> componentType = component.getType();

         if(componentType == null) {
            try {
               FunctionCall call = component.getCreateMethod(delegate);
               Method method = call.getMethod();
               
               componentType = method.getReturnType();               
            } catch(Exception e) {
               throw new IllegalStateException("Could not determine type of component " + component);
            }
         }
         if(type.isAssignableFrom(componentType)) {
           String name = component.getName();
            T value = lookup(name);
            
            matches.add(value);         
         }
      }
      for(Module module : modules) {
         Set<T> additional = module.lookupAll(type);
         matches.addAll(additional);
      }
      Set<String> registeredNames = registryByName.keySet();

      for(String registeredName : registeredNames) {
         if(!matches.contains(registeredName)) {
            Object object = registryByName.get(registeredName);
            Class<?> objectType = object.getClass();

            if(type.isAssignableFrom(objectType)) {
               matches.add((T)object);
            }
         }
      }
      return matches;  
   }

   @Override
   public <T> T lookup(Class<T> type) {
      Object value = registryByClass.get(type);

      if(value == null) {
         if(excludedTypes.contains(type)) {
            throw new IllegalStateException("Could not lookup a specific component of " + type + " as many are of that type");
         }
         LinkedList<Component> matchingComponents = new LinkedList<Component>();
         LinkedList<Component> similarComponents = new LinkedList<Component>();

         for(Component component : beans) {
            Class componentType = component.getType();
  
            if(componentType == null) { // could be a factory
               try {
                  FunctionCall call = component.getCreateMethod(delegate);
                  Method method = call.getMethod();
                  
                  componentType = method.getReturnType();               
               } catch(Exception e) {
                  throw new IllegalStateException("Could not determine type of component " + component);
               }
            }
            if(componentType == type) {
               matchingComponents.add(component);
            }
            if(type.isAssignableFrom(componentType)) {
               similarComponents.add(component);
            }            
         }
         if(matchingComponents.isEmpty() ) {
            matchingComponents.addAll(similarComponents);
         }
         if(matchingComponents.isEmpty()) {
            for(Module module : modules) {
               T moduleValue = module.lookup(type);
               
               if(moduleValue != null) {
                  return moduleValue;
               }
            }
            throw new IllegalStateException("Could not find component of "+ type);
         }
         if(matchingComponents.size() > 1) {
            throw new IllegalStateException("More than one component of " + type + " was found");
         }
         Component component = matchingComponents.getFirst();
         String name = component.getName();
         Object result = lookup(name);

         if(result != null) {
            registryByClass.put(type, result);
         }
         return (T) result;
      }
      return (T) value;
   }

   @Override
   public <T> T lookup(String name) {
      Object value = find(name);

      if(value != null) {
         ComponentValue componentValue = resolveComponent(name);

         if(componentValue != null) {
            try {
               value = componentValue.getValue();
            }catch(Exception e) {
               throw new IllegalStateException("Could not lookup component " + name, e);
            }
         }
      }
      return (T)value;
   }

   private Object find(String name) {
      Object value = findInstance(name);

      if(value == null) {
         ComponentValue componentValue = resolveComponent(name);

         if(componentValue == null) {
            try {
               CollectionComponent collectionComponent = resolveCollection(name);

               if(collectionComponent == null) {
                  throw new IllegalStateException("Component '" + name + "' does not exist");
               }
               return collectionComponent.getValue(delegate);
            } catch(Exception e) {
               throw new IllegalStateException("Component '" + name + "' does not exist so attempted to find collection which failed", e);
            }
         }
         try {
            Component component = componentValue.getComponent();
            String variableId = component.getVariableName();
            ContainerTracer tracer = getTracer();
            
            value = componentValue.getValue();

            FunctionCall stopMethod = component.getStopMethod(delegate);

            if(stopMethod != null) {
               Method actualMethod = stopMethod.getMethod();
               List<FunctionParameter> parameters = stopMethod.getParameters();
               List<String> variables = new LinkedList<String>();

               for(FunctionParameter parameter : parameters) {
                  String parameterValue = parameter.getValue();

                  if(parameter.isQuote()) {
                     variables.add("\"" + parameterValue +"\"");
                  } else {
                     variables.add(parameterValue);
                  }
               }
               tracer.registerStopMethod(variableId, actualMethod, variables);
            }
         } catch(Exception e) {
            throw new IllegalStateException("Could not load component " + name, e);
         }
      }
      return value;
   }

   private Object findInstance(String name) {
      Object value = registryByFactory.get(name);

      if(value == null) {
         value = registryByName.get(name);

         if(value == null) {
            Component component = beans.get(name);

            if(component == null && parent != null) {
               return parent.findInstance(name);
            }
         }
      }
      return value;
   }

   private ComponentValue findComponentInCurrentContext(String name) {
      Component component = beans.get(name);

      if(component != null) {
         Object value = registryByName.get(name);
         Object existingFactory = registryByFactory.get(name);

         if(value == null) {
            try {
               String factory = component.getFactory();
               Component factoryComponent = component;
               
               if(factory != null) {
                  ComponentValue factoryComponentValue = resolveComponent(factory);
                  
                  factoryComponent = factoryComponentValue.getComponent();
                  value = factoryComponent.getInstance(delegate);
               }  else {
                  value = component.getInstance(delegate);               
                  component.doProcessProperties(value, delegate);
               }
               //FunctionCall startMethod = component.getStartMethod(delegate);

               //if(startMethod != null) {
               //   if(!started.contains(component)) {
               //      String variableName = component.getVariableName();
               //      startMethod.call(variableName, null, delegate, value);
               //      started.add(component);
               //   }
               //}
               FunctionCall method = component.getCreateMethod(delegate);

               if(method != null) {                
                  if(existingFactory == null) {
                     String assignToName = component.getResultVariableName();
                     String variableName = factoryComponent.getVariableName();
                     Object result = method.call(variableName, assignToName, delegate, value);

                     if(value != null) {
                        register(name, result);
                        registerByType(value);
                        registerByFactory(name, value);
                     }
                     return new ComponentValue(component, result, value);
                  }
                  return new ComponentValue(component, value, existingFactory);
               } else {
                  register(name,  value);
               }
            } catch(Exception e) {
               throw new IllegalStateException("Could not instantiate " + name, e);
            }
         }
         return new ComponentValue(component, value, existingFactory);
      }
      return null;
   }

   protected ComponentValue resolveComponent(String name) {
      return resolveComponent(name, new HashSet<Object>());
   }

   private ComponentValue resolveComponent(String name, Set<Object> ignore) {
      ComponentValue componentValue = findComponentInCurrentContext(name);

      if(componentValue == null) {
         List<String> outOfScope = new ArrayList<String>();

         ignore.add(this);

         for(Module module : modules) {
            if(!ignore.contains(module)) {
               ModuleContainer container = (ModuleContainer)module;
               ComponentValue moduleValue = container.resolveComponent(name, ignore);

               if(moduleValue != null) {
                  Component moduleComponent = moduleValue.getComponent();
                  Scope scope = moduleComponent.getScope();
                  String moduleName = module.getName();

                  if(scope.isPublic()) {
                     return moduleValue;
                  }
                  outOfScope.add(moduleName);
               }
            }
         }
         if(!outOfScope.isEmpty()) {
            throw new IllegalStateException("Found component " + name + " but was not public in modules " + outOfScope);
         }
      }
      if(componentValue == null && parent != null) {
         if(!ignore.contains(parent)) {
            return parent.resolveComponent(name, ignore);
         }
      }
      return componentValue;
   }

   protected CollectionComponent resolveCollection(String name) {
      CollectionComponent component = resolveMap(name);

      if(component == null) {
         component = resolveList(name);
      }
      if(component == null) {
         component = resolveSet(name);
      }
      return component;
   }
   
   protected PropertiesComponent resolveProperties(String name) {
      return resolveProperties(name, new HashSet<Object>());
   }

   private PropertiesComponent resolveProperties(String name, Set<Object> ignore) {
      PropertiesComponent component = properties.get(name);

      if(component == null) {
         List<String> outOfScope = new ArrayList<String>();

         ignore.add(this);

         for(Module module : modules) {
            if(!ignore.contains(module)) {
               ModuleContainer container = (ModuleContainer)module;
               PropertiesComponent moduleComponent = container.resolveProperties(name, ignore);

               if(moduleComponent != null) {
                  Scope scope = moduleComponent.getScope();
                  String moduleName = module.getName();

                  if(scope.isPublic()) {
                     return moduleComponent;
                  }
                  outOfScope.add(moduleName);
               }
            }
         }
         if(!outOfScope.isEmpty()) {
            throw new IllegalStateException("Found properties " + name + " but was not public in modules " + outOfScope);
         }
      }
      if(component == null && parent != null) {
         if(!ignore.contains(parent)) {
            return parent.resolveProperties(name, ignore);
         }
      }
      return component;
   }   

   protected MapComponent resolveMap(String name) {
      return resolveMap(name, new HashSet<Object>());
   }

   private MapComponent resolveMap(String name, Set<Object> ignore) {
      MapComponent component = maps.get(name);

      if(component == null) {
         List<String> outOfScope = new ArrayList<String>();

         ignore.add(this);

         for(Module module : modules) {
            if(!ignore.contains(module)) {
               ModuleContainer container = (ModuleContainer)module;
               MapComponent moduleComponent = container.resolveMap(name, ignore);

               if(moduleComponent != null) {
                  Scope scope = moduleComponent.getScope();
                  String moduleName = module.getName();

                  if(scope.isPublic()) {
                     return moduleComponent;
                  }
                  outOfScope.add(moduleName);
               }
            }
         }
         if(!outOfScope.isEmpty()) {
            throw new IllegalStateException("Found map " + name + " but was not public in modules " + outOfScope);
         }
      }
      if(component == null && parent != null) {
         if(!ignore.contains(parent)) {
            return parent.resolveMap(name, ignore);
         }
      }
      return component;
   }

   protected ListComponent resolveList(String name) {
      return resolveList(name, new HashSet<Object>());
   }

   private ListComponent resolveList(String name, Set<Object> ignore) {
      ListComponent component = lists.get(name);

      if(component == null) {
         List<String> outOfScope = new ArrayList<String>();

         ignore.add(this);

         for(Module module : modules) {
            if(!ignore.contains(module)) {
               ModuleContainer container = (ModuleContainer)module;
               ListComponent moduleComponent = container.resolveList(name, ignore);

               if(moduleComponent != null) {
                  Scope scope = moduleComponent.getScope();
                  String moduleName = module.getName();

                  if(scope.isPublic()) {
                     return moduleComponent;
                  }
                  outOfScope.add(moduleName);
               }
            }
         }
         if(!outOfScope.isEmpty()) {
            throw new IllegalStateException("Found list " + name + " but was not public in modules " + outOfScope);
         }
      }
      if(component == null && parent != null) {
         if(!ignore.contains(parent)) {
            return parent.resolveList(name, ignore);
         }
      }
      return component;
   }

   protected SetComponent resolveSet(String name) {
      return resolveSet(name, new HashSet<Object>());
   }

   private SetComponent resolveSet(String name, Set<Object> ignore) {
      SetComponent component = sets.get(name);

      if(component == null) {
         List<String> outOfScope = new ArrayList<String>();

         ignore.add(this);

         for(Module module : modules) {
            if(!ignore.contains(module)) {
               ModuleContainer container = (ModuleContainer)module;
               SetComponent moduleComponent = container.resolveSet(name, ignore);

               if(moduleComponent != null) {
                  Scope scope = moduleComponent.getScope();
                  String moduleName = module.getName();

                  if(scope.isPublic()) {
                     return moduleComponent;
                  }
                  outOfScope.add(moduleName);
               }
            }
         }
         if(!outOfScope.isEmpty()) {
            throw new IllegalStateException("Found set " + name + " but was not public in modules " + outOfScope);
         }
      }
      if(component == null && parent != null) {
         if(!ignore.contains(parent)) {
            return parent.resolveSet(name, ignore);
         }
      }
      return component;
   }

   public ContainerTracer getTracer() {
      if(parent != null) {
         return parent.getTracer();
      }
      return tracer;
   }
   
   @Override
   public String toString() {
      return source;
   }
}
