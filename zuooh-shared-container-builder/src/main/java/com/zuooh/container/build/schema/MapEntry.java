package com.zuooh.container.build.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;
import org.simpleframework.xml.core.Validate;
import org.simpleframework.xml.util.Entry;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.common.util.StringConverter;

@Root(name="entry")
public class MapEntry implements Entry {

   @Attribute(name="key")
   protected String key;   

   @Attribute(name="value", required=false)
   protected String value;   

   @Attribute(name="value-ref", required=false)
   protected String ref;

   @Element(name="bean", required=false)
   protected Bean bean;
   
   @Element(name="value", required=false)
   protected String text;
   
   @Transient
   protected StringConverter converter;

   public MapEntry() {
      this.converter = new StringConverter();
   }   

   @Validate
   private void validate() {
      String[] names = {"value", "value-ref", "bean"};
      Object[] values = {value, ref, bean};
      String previous = null;

      for(int i = 0; i < names.length; i++) {
         if(values[i] != null) {
            if(previous != null) {
               throw new IllegalStateException("Cannot declare '" +previous+ "' with '"+names[i]+"' in an argument");
            }
            previous = names[i];
         }
      }
      if(value != null && text != null) {
         throw new IllegalStateException("Map entry cannot have a 'value' attribute and a 'value' element");
      }
   }   

   @Override
   public String getName() {
      return key;
   }     

   public String getKeyVariable(Class type) {
      if(type == String.class) {
         return String.format("\"%s\"", key);
      }
      if(type == Class.class) {
         return String.format("%s.class", key);
      }
      return key;
   } 

   public String getVariableName(ContainerContext container, Class type) throws Exception {
      if(ref != null) {
         ScopedComponent component = container.componentFor(ref);

         if(component == null) {
            return PropertyMapper.getPropertyName(ref);
         }
         return component.getResultVariableName();
      }
      if(bean != null) {
         return bean.getResultVariableName();
      }
      String token = value;
      
      if(token == null) {
         token = text;
      }
      if(type == String.class) {
         return String.format("\"%s\"", token);
      }
      if(type == Object.class) {
         return String.format("\"%s\"", token); // untyped is treated as string
      }
      if(type == Class.class) {
         return String.format("%s.class", token);
      }
      return token;
   }

   public Object getValue(ContainerContext container, Class type) throws Exception {
      String token = value;
      
      if(token == null) {
         token = text;
      }
      if(token != null) {
         if(type == Object.class) {
            return token;
         }
         return converter.convert(type, token);
      }
      if(ref != null) {
         return container.lookupComponent(ref);
      }
      return bean.getInstance(container);
   }

   @Override
   public String toString() {
      return ref;
   }
}
