package com.zuooh.container.build.schema;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.zuooh.container.ContainerTracer;

interface ContainerContext {
   boolean isComponent(String name);
   boolean isProperties(String name);   
   boolean isMap(String name);
   boolean isList(String name);
   boolean isSet(String name);
   ScopedComponent componentFor(String name) throws Exception;
   Object lookupComponent(String name) throws Exception;
   Properties lookupProperties(String name) throws Exception;   
   Map<Object, Object> lookupMap(String name, Class[] dependents) throws Exception;
   Set<Object> lookupSet(String name, Class[] dependents) throws Exception;
   List<Object> lookupList(String name, Class[] dependents) throws Exception;
   FunctionCall lookupFunction(String function, Class type) throws Exception;
   ContainerTracer lookupTracer();
}
