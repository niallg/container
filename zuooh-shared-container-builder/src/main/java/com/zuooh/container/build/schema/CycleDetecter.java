package com.zuooh.container.build.schema;

import java.util.LinkedList;

public class CycleDetecter extends ThreadLocal<LinkedList<String>> {

   public LinkedList<String> initialValue() {
      return new LinkedList<String>();
   }

   public void popFromPath(String name) {
      LinkedList<String> searchPath = get();
      String removed = searchPath.removeLast();

      if(!removed.equals(name)) {
         throw new IllegalStateException("Search path is inconsistent");
      }
   }

   public void pushToPath(String name) {
      LinkedList<String> searchPath = get();

      if(searchPath.contains(name)) {
         StringBuilder builder = new StringBuilder("The following search path contains a cycle: ");

         for(String component : searchPath) {
            builder.append("'");
            builder.append(component);
            builder.append("' -> ");
         }
         builder.append("'");
         builder.append(name);
         builder.append("'");
         searchPath.addLast(name);
         throw new IllegalStateException("Search path contains a cycle for " + builder);
      }
      searchPath.addLast(name);
   }



   public void clear() {
      get().clear();
   }
}
