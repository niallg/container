package com.zuooh.container.build.schema;

public interface CollectionComponent extends ScopedComponent {
   Object getValue(ContainerContext container) throws Exception;
   Object getValue(ContainerContext container, Class[] dependents) throws Exception;
}
