package com.zuooh.container.build.schema;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.container.ContainerTracer;

@Root(name="list")
public class ListComponent implements CollectionComponent {

   @Attribute(required=false)
   private String id;

   @Attribute(required=false)
   private String scope;

   @Attribute(required=false)
   private String parent;

   @ElementListUnion({
      @ElementList(entry="value", type=CollectionValue.class, inline=true, required=false),
      @ElementList(entry="ref", type=CollectionReference.class, inline=true, required=false),
      @ElementList(entry="bean", type=CollectionBean.class, inline=true, required=false)
   })
   private ArrayList<CollectionElement> list;

   @Attribute(name="list-class", required=false)
   private Class use;
   
   @Attribute(name="value-type", required=false)
   private Class value;
   
   @Transient
   private List<Object> cache;

   public ListComponent() {
      super();
   }

   @Override
   public String getVariableName() {
      if(id != null) {
         return PropertyMapper.getPropertyName(id) + System.identityHashCode(this);
      }
      return String.format("list" + System.identityHashCode(this));
   }

   @Override
   public String getResultVariableName() {
      return getVariableName();
   }

   @Override
   public List<Object> getValue(ContainerContext container) throws Exception {
      Class[] type = new Class[]{String.class};

      if(value != null) {
         type[0] = value;
      }
      return getValue(container, type);
   }

   @Override
   public List<Object> getValue(ContainerContext container, Class[] dependents) throws Exception {
      if(cache == null) {
         List<Object> value = new LinkedList<Object>();
         
         if(use != null) {
            if(List.class.isAssignableFrom(use)) {
               throw new IllegalStateException("The list-class " + use + " does not implement java.util.List");
            }
            value = (List<Object>)use.newInstance();
         }
         ContainerTracer tracer = container.lookupTracer();
         String variableName = getVariableName();
   
         if(parent != null) {
            List<Object> list = container.lookupList(parent, dependents);
            ScopedComponent component = container.componentFor(parent);
            String extendId = component.getVariableName();
   
            value.addAll(list);
            tracer.newListCreated(variableName, extendId, dependents);
         } else {
            tracer.newListCreated(variableName, null, dependents);
         }
         if(list != null) {
            for(CollectionElement entry : list) {
               String entryVariableName = entry.getVariableName(container, dependents[0]);
      
               value.add(entry.getValue(container, dependents[0]));
               tracer.addEntryToCollection(variableName, entryVariableName);
            }
         }
         cache = value;
      }
      return cache;
   }

   @Override
   public Scope getScope() {
      return Scope.resolveScope(scope);
   }

   @Override
   public String getName() {
      return id;
   }
}
