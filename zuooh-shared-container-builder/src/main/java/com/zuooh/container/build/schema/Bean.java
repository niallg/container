package com.zuooh.container.build.schema;

import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.core.Validate;

import com.zuooh.common.reflect.Introspector;
import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.common.util.StringConverter;
import com.zuooh.container.ContainerTracer;

@Root
public class Bean {

   @Attribute(name="class", required=false)
   protected Class type;

   @ElementList(entry="constructor-arg", inline=true, required=false)
   protected List<Argument> arguments;

   @ElementList(entry="property", inline=true, required=false)
   protected List<Property> properties;

   @Transient
   protected StringConverter converter;
   
   @Transient
   protected Object cache;

   public Bean() {
      this.converter = new StringConverter();
   }
   
   @Validate
   public void validate() {
      if(type == null) {
         throw new IllegalStateException("Bean must have type attribute");
      }
   }
   
   public Class getType(){
      return type;
   }
   
   public String getName() {
      return null;
   }
  
   public String getVariableName() {
      return getResultVariableName();
   }

   public String getResultVariableName() {
      String objectName = type.getSimpleName();
      String variableName = PropertyMapper.getPropertyName(objectName);

      return String.format(variableName + System.identityHashCode(this));
   }

   public Object getInstance(ContainerContext context) throws Exception {
      if(cache == null) {
         String id = getName();
         ContainerTracer tracer = context.lookupTracer();
         Constructor factory = getConstructor(context);
         Class[] params = factory.getParameterTypes();
         Object[] values = new Object[params.length];
         List<String> variables = new LinkedList<String>();
   
         for(int i = 0; i < params.length; i++) {
            Class actual = params[i];
            Argument argument = arguments.get(i);
            Class defined = argument.getType(context);
            String variable = argument.getVariable(context, actual);
   
            if(defined != null) {
               if(actual.isAssignableFrom(defined)) {
                  Class[] dependents = Introspector.getParameterDependents(factory, i);
                  Object value = argument.getValue(context, dependents);
   
                  values[i] = value;
               }
            } else {
               if(converter.accept(actual)) {
                  Class[] dependents = Introspector.getParameterDependents(factory, i);
                  Object text = argument.getValue(context, dependents);
                  Object value = converter.convert(actual, String.valueOf(text));
   
                  values[i] = value;
               } else {
                  throw new IllegalStateException("Could not transform '" + actual + "' for '" + id + "'");
               }
            }
            variables.add(variable);
         }
         if(!factory.isAccessible()) {
            factory.setAccessible(true);
         }
         String thisName = getVariableName();
         tracer.newInstanceCreated(thisName, factory, variables);
         
         if(id != null) {
            tracer.registerComponent(id, thisName);
         }
         cache = factory.newInstance(values);
      }
      return cache;

   }

   public void doProcessProperties(Object instance, ContainerContext context) throws Exception {
      List<String> propertiesRequired = new LinkedList<String>();

      if(properties != null) {
         String id = getName();
         ContainerTracer tracer = context.lookupTracer();
         String variableId = getVariableName();

         for(Property property : properties) {
            String name = property.getName();
            propertiesRequired.add(name);
         }
         Class examine = type;

         while(examine != null && !propertiesRequired.isEmpty()) {
            Method[] list = type.getDeclaredMethods();

            for(int i = 0; i < list.length; i++) {
               Method method = list[i];
               Class[] parameterTypes = method.getParameterTypes();

               if(parameterTypes.length == 1) {
                  for(Property property : properties) {
                     String name = property.getName();

                     if(propertiesRequired.contains(name)) {
                        String propertyName = getMethodName(name);
                        String methodName = method.getName();

                        if(propertyName.equals(methodName)) {
                          Class parameter = parameterTypes[0];
                          Class propertyType = property.getType(context);

                          if(!method.isAccessible()) {
                             method.setAccessible(true);
                          }
                          if(propertyType != null) {
                             if(parameter.isAssignableFrom(propertyType)) {
                                Class[] dependents = Introspector.getParameterDependents(method, i);
                                Object value = property.getValue(context, dependents);
                                Class type = value.getClass();

                                method.invoke(instance, value);
                                propertiesRequired.remove(name);

                                String variable = property.getVariable(context, type);
                                tracer.callMethod(variableId, null, method, Collections.singletonList(variable));
                                break;
                             }
                          } else {
                             if(converter.accept(parameter)) {
                                Class[] dependents = Introspector.getParameterDependents(method, i);
                                Object text = property.getValue(context, dependents);
                                Object value = converter.convert(parameter, String.valueOf(text));
                                Class type = value.getClass();

                                method.invoke(instance, value);
                                propertiesRequired.remove(name);

                                String variable = property.getVariable(context, type);
                                tracer.callMethod(variableId, null, method, Collections.singletonList(variable));
                                break;
                             }
                          }
                        }
                     }
                  }
               }
            }
            examine = examine.getSuperclass();
         }
         if(!propertiesRequired.isEmpty()) {
            throw new IllegalStateException("The following properties could not be matched in '" +id + "' "+propertiesRequired);
         }
      }
   }

   private Constructor getConstructor(ContainerContext context) throws Exception {
      String id = getName();
      Constructor[] list = type.getDeclaredConstructors();
      List<Constructor> valid = new LinkedList<Constructor>();

      for(Constructor factory : list) {
         Class[] params = factory.getParameterTypes();
         if(arguments != null) {
            if(params.length == arguments.size()) {
               int matchCount = 0;

               for(int i = 0; i < params.length; i++) {
                  Class actual = params[i];
                  Argument argument = arguments.get(i);
                  Class defined = argument.getType(context);

                  if(defined != null) {
                     if(actual.isAssignableFrom(defined)) {
                        matchCount++;
                     }
                  } else {
                     if(converter.accept(actual)) {
                        matchCount++;
                     }
                  }
               }
               if(matchCount == params.length) {
                  valid.add(factory);
               }
            }
         } else if(params.length == 0) {
            valid.add(factory);
            break;
         }
      }
      if(valid.size() == 0) {
         StringWriter writer = new StringWriter();
         Persister persister = new Persister();

         if(arguments != null) {
            for(Argument argument : arguments) {
               persister.write(argument, writer);
               writer.write("\n");
            }
         }
         if(id != null) {
            throw new IllegalStateException("Could not find matching constructor for '" + id + "' of type '" + type.getName() + "' using arguments:\n" + writer);
         } else {
            throw new IllegalStateException("Could not find matching constructor for type '" + type.getName() + "' using arguments:\n" + writer);
         }
      }
      if(valid.size() > 1) {
         for(Constructor factory : valid) {
            Class[] params = factory.getParameterTypes();
            
            if(params.length == 1 && params[0] == String.class) { // choose most generic
               return factory;
            }
         }
         if(id != null) {
            throw new IllegalStateException("Ambiguous constructors found for '" + id + "' of type '" + type.getName() + "'");
         } else {
            throw new IllegalStateException("Ambiguous constructors found for type '" + type.getName() + "'");
         }
      }
      return valid.get(0);
   }

   @Override
   public String toString() {
      return String.format("anonymous component of %s", type);
   }

   protected static String getMethodName(String name) {
      return "set" + getName(name);
   }

   protected static String getName(String name) {
      int length = name.length();

      if(length > 0) {
         char[] array = name.toCharArray();
         char first = array[0];

         if(isLowerCase(first)) {
            array[0] = toUpperCase(first);
         }
         return new String(array);
      }
      return name;
   }

   protected static char toUpperCase(char value) {
      return Character.toUpperCase(value);
   }

   protected static boolean isLowerCase(char value) {
      return Character.isLowerCase(value);
   }
}
