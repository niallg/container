package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class AddToCollectionSnippet extends BasicSnippet {

   private final String collection;
   private final String variable;
   private final String uniqueKey;

   public AddToCollectionSnippet(String collection, String variable) {
      this.uniqueKey = String.format("%s.%s", collection, variable);
      this.collection = collection;
      this.variable = variable;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      String alreadyDone = state.get(uniqueKey);

      if(!variable.equals(alreadyDone)) {
         builder.append("      ");
         builder.append(collection);
         builder.append(".add(");
         builder.append(variable);
         builder.append(");\n");
         state.put(uniqueKey, variable);
      }
   }

}
