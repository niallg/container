package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class AddToPropertiesSnippet extends BasicSnippet {

   private final String properties;
   private final String key;
   private final String variable;
   private final String uniqueKey;

   public AddToPropertiesSnippet(String properties, String key, String variable) {
      this.uniqueKey = String.format("%s.%s", properties, key);
      this.properties = properties;
      this.key = key;
      this.variable = variable;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      String alreadyDone = state.get(uniqueKey);

      if(!variable.equals(alreadyDone)) {
         builder.append("      ");         
         builder.append(properties);
         builder.append(".setProperty(\"");
         builder.append(key);
         builder.append("\", \"");
         builder.append(variable);
         builder.append("\");\n");
         state.put(uniqueKey, variable);
      }
   }
}
