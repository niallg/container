package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class RegisterComponentSnippet extends BasicSnippet {

   private final String registrationId;
   private final String variableId;

   public RegisterComponentSnippet(String registrationId, String variableId) {
      this.registrationId = registrationId;
      this.variableId = variableId;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      builder.append("      register(\"");
      builder.append(registrationId);
      builder.append("\", ");
      builder.append(variableId);
      builder.append(");\n");
   }
}
