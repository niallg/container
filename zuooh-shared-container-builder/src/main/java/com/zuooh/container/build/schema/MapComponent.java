package com.zuooh.container.build.schema;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.common.util.StringConverter;
import com.zuooh.container.ContainerTracer;

@Root(name="map")
public class MapComponent implements CollectionComponent {

   @Attribute(required=false)
   private String id;

   @Attribute(required=false)
   private String scope;

   @Attribute(required=false)
   private String parent;

   @Attribute(name="map-class", required=false)
   private Class use;
   
   @Attribute(name="key-type", required=false)
   private Class key;

   @Attribute(name="value-type", required=false)
   private Class value;

   @ElementList(entry="entry", inline=true, required=false)
   private ArrayList<MapEntry> map;

   @Transient
   private StringConverter converter;
   
   @Transient
   private Map<Object, Object> cache;

   public MapComponent() {
      this.converter = new StringConverter();
   }

   @Override
   public String getVariableName() {
      if(id != null) {
         return PropertyMapper.getPropertyName(id) + System.identityHashCode(this);
      }
      return String.format("map" + System.identityHashCode(this));
   }

   @Override
   public String getResultVariableName() {
      return getVariableName();
   }

   @Override
   public Map<Object, Object> getValue(ContainerContext container) throws Exception {
      Class[] types = new Class[]{String.class, String.class};

      if(key != null) {
         types[0] = key;
      }
      if(value != null) {
         types[1] = value;
      }
      return getValue(container, types);
   }

   @Override
   public Map<Object, Object> getValue(ContainerContext container, Class[] dependents) throws Exception {
      if(cache == null) {
         Map<Object, Object> value = new LinkedHashMap<Object, Object>();        
         
         if(use != null) {
            if(Map.class.isAssignableFrom(use)) {
               throw new IllegalStateException("The map-class " + use + " does not implement java.util.Map");
            }
            value = (Map<Object, Object>)use.newInstance();
         }
         ContainerTracer tracer = container.lookupTracer();
         String variableName = getVariableName();
   
         if(parent != null) {
            Map<Object, Object> map = container.lookupMap(parent, dependents);
            ScopedComponent component = container.componentFor(parent);
            String extendId = component.getVariableName();
   
            value.putAll(map);
            tracer.newMapCreated(variableName, extendId, dependents);
         } else {
            tracer.newMapCreated(variableName, null, dependents);
         }
         if(map != null) {
            for(MapEntry entry : map) {
               String keyVariableName = entry.getKeyVariable(dependents[0]);
               String entryVariableName = entry.getVariableName(container, dependents[1]);
               Object key = entry.key;
      
               if(dependents[0] != Object.class) {
                  key = converter.convert(dependents[0], entry.key);
               }
               Object mapValue = entry.getValue(container, dependents[1]);
               
               if(mapValue == null) {
                  throw new IllegalStateException("Could not find entry " + entry.getName() + " for map " + getName());
               }
               value.put(key, mapValue);
               tracer.addEntryToMap(variableName, keyVariableName, entryVariableName);
            }
         }
         cache = value;
      }
      return cache;
   }

   @Override
   public Scope getScope() {
      return Scope.resolveScope(scope);
   }

   @Override
   public String getName() {
      return id;
   }
}
