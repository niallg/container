package com.zuooh.container.build.schema;

public enum Scope {
   PUBLIC("public"),
   PRIVATE("private"),
   NONE("none");

   private final String token;

   private Scope(String token) {
      this.token = token;
   }

   public boolean isDefined() {
      return this != NONE;
   }

   public boolean isPublic() {
      return this != PRIVATE;
   }

   public static Scope resolveScope(String token) {
      if(token != null) {
         for(Scope scope : values()) {
            if(scope.token.equalsIgnoreCase(token)) {
               return scope;
            }
         }
      }
      return NONE;
   }
}
