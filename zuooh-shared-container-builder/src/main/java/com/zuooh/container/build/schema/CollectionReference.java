package com.zuooh.container.build.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import com.zuooh.common.reflect.PropertyMapper;

@Root
public class CollectionReference implements CollectionElement {

   @Attribute
   private String bean;

   @Override
   public Object getValue(ContainerContext container, Class type) throws Exception {    
      return container.lookupComponent(bean);
   }
   
   @Override
   public String getVariableName(ContainerContext container, Class type) throws Exception {
      ScopedComponent component = container.componentFor(bean);

      if(component == null) {
         return PropertyMapper.getPropertyName(bean);
      }
      return component.getResultVariableName();
   }
}
