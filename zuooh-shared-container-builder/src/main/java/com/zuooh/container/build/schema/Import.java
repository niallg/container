package com.zuooh.container.build.schema;

import java.io.InputStream;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.util.Entry;

import com.zuooh.common.FileSystem;

@Root
class Import implements Entry {

   @Attribute
   private String resource;
   
   @Attribute(required=false)
   private boolean ignore;

   @Override
   public String getName() {
      return resource;
   }

   public ModuleContainer getContainer(FileSystem fileSystem, Serializer serializer) throws Exception {
      if(!ignore) {
         InputStream source = fileSystem.openFile(resource);
   
         try {
            return serializer.read(ModuleContainer.class, source);
         } catch(Exception e) {
            throw new IllegalStateException("Error proccessing import '" +resource+ "'", e);
         }
      }
      return new ModuleContainer();
   }

   public String toString(){
      return resource;
   }
}
