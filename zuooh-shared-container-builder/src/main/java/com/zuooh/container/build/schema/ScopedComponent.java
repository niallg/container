package com.zuooh.container.build.schema;

import org.simpleframework.xml.util.Entry;

public interface ScopedComponent extends Entry {
   String getResultVariableName();
   String getVariableName();
   Scope getScope();
}
