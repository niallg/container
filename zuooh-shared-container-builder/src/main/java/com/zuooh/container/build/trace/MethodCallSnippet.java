package com.zuooh.container.build.trace;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MethodCallSnippet implements ContainerSnippet {

   private final String id;
   private final String assignToId;
   private final Method method;
   private final List<String> variables;

   public MethodCallSnippet(String id, String assignToId, Method method, List<String> variables) {
      this.id = id;
      this.assignToId = assignToId;
      this.method = method;
      this.variables = variables;
   }

   @Override
   public void assignments(Map<String, Class> typeInformationOnVariables) throws Exception {
      Class[] parameterTypes = method.getParameterTypes();

      for(int i = 0; i < parameterTypes.length; i++) {
         String variable = variables.get(i);
         Class parameterType = parameterTypes[i];

         if(parameterType != String.class && parameterType != Class.class) {
            Class existingType = typeInformationOnVariables.get(variable);

            if(existingType != null) {
               if(existingType.isAssignableFrom(parameterType)) {
                  typeInformationOnVariables.put(variable, parameterType);
               }
            } else {
               typeInformationOnVariables.put(variable, parameterType);
            }
         }
      }
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      if(assignToId != null) {
         renderReturn(importList, builder, typeInformationOnVariables);
      }
      Class[] parameterTypes = method.getParameterTypes();

      for(Class parameterType : parameterTypes) {
         Class parentType = parameterType.getSuperclass();

         if(parentType != null && parentType.isEnum()) {
            String enumClassName = parentType.getName();
            importList.add("static " + enumClassName + ".*");
         } else if(parameterType.isEnum()) {
            String enumClassName = parameterType.getName();
            importList.add("static " + enumClassName + ".*");
         }
      }
      String methodName = method.getName();
      
      if(assignToId == null) {
         builder.append("      ");
      }
      builder.append(id);
      builder.append(".");
      builder.append(methodName);
      builder.append("(");

      if(variables != null) {
         renderParameters(builder);
      }
      builder.append(");\n");
   }

   private String determineReturnType(Set<String> importList, Map<String, Class> typeInformationOnVariables) throws IOException {
      Class returnType = method.getReturnType();
      Type type = method.getGenericReturnType();

      if(type instanceof TypeVariable) {
         TypeVariable typeVariable = (TypeVariable)type;
         String name = typeVariable.getName();

         if(name.equals("T") && returnType == Object.class) {
            Class bestKnownInfo = typeInformationOnVariables.get(assignToId);

            if(bestKnownInfo != null) {
               String returnTypeClassName = bestKnownInfo.getName();
               importList.add(returnTypeClassName);
               return bestKnownInfo.getSimpleName();
            }
         }
      }
      String returnTypeClassName = returnType.getName();
      importList.add(returnTypeClassName);
      return returnType.getSimpleName();
   }

   private void renderReturn(Set<String> importList, Appendable builder, Map<String, Class> typeInformationOnVariables) throws IOException {
      String returnTypeObjectName = determineReturnType(importList, typeInformationOnVariables);

      builder.append("      final ");
      builder.append(returnTypeObjectName);
      builder.append(" ");
      builder.append(assignToId);
      builder.append(" = ");
   }

   private void renderParameters(Appendable builder) throws IOException {
      Class[] parameterTypes = method.getParameterTypes();
      String prefix = "";

      for(int i = 0; i < parameterTypes.length; i++) {
         Class parameterType = parameterTypes[i];
         String variable = variables.get(i);

         builder.append(prefix);

         if(parameterType == String.class) {
            if(!variable.startsWith("\"")) {
               builder.append("\"");
            }
            builder.append(variable);

            if(!variable.endsWith("\"")) {
               builder.append("\"");
            }
         } else if(parameterType == Class.class) {
            builder.append(variable);
            builder.append(".class");
         } else {
            builder.append(variable);
         }
         prefix = ", ";
      }
   }

   @Override
   public String toString(){
      return method.toString();
   }
}
