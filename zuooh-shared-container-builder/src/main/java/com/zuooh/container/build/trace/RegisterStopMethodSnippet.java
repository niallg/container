package com.zuooh.container.build.trace;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RegisterStopMethodSnippet implements ContainerSnippet {

   private final MethodCallSnippet methodCall;

   public RegisterStopMethodSnippet(String id, Method method, List<String> variables) {
      this.methodCall = new MethodCallSnippet(id, null, method, variables);
   }

   @Override
   public void assignments(Map<String, Class> typeInformationOnVariables) throws Exception {
      methodCall.assignments(typeInformationOnVariables);
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      builder.append("      registerStopFunction(new Runnable() {\n");
      builder.append("         public void run() {\n");
      builder.append("            try {\n");
      methodCall.render(importList, sourceFiles, builder, state, typeInformationOnVariables);
      builder.append("            }catch(Exception e) {\n");
      builder.append("               LOG.info(e, \"Could not stop X\");\n");
      builder.append("            }\n");
      builder.append("         }\n");
      builder.append("      });\n");
   }
}
