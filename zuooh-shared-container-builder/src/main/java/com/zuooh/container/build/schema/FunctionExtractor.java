package com.zuooh.container.build.schema;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import com.zuooh.common.function.FunctionParameter;
import com.zuooh.common.function.FunctionParser;
import com.zuooh.common.function.FunctionProcessor;
import com.zuooh.container.Container;

public class FunctionExtractor {

   private final Map<String, Method> methodCache;
   private final FunctionProcessor processor;
   private final Container container;

   public FunctionExtractor(Container container) {
      this.methodCache = new ConcurrentHashMap<String, Method>();
      this.processor = new FunctionProcessor();
      this.container = container;
   }

   public FunctionCall extractFunction(String function, Class type) {
      FunctionParser parser = processor.processFunction(function);
      List<FunctionParameter> parameters = parser.getParameters();
      String name = parser.getName();
      Method method = resolveMethod(name, type, parameters);

      return new FunctionCall(parameters, method);
   }

   private Method resolveMethod(String name, Class type, List<FunctionParameter> parameters) {
      int size = parameters.size();
      String key = createMethodKey(name, type, parameters);
      Method method = methodCache.get(key);

      if(method != null) {
         return method;
      }
      while(type != null) {
         Method[] methods = type.getDeclaredMethods();

         for(int i = 0; i < methods.length; i++) {
            Class[] types = methods[i].getParameterTypes();

            if(types.length == size) {
               String methodName = methods[i].getName();

               if(name.equals(methodName)) {
                  boolean signatureMatch = true;

                  for(int j = 0; j < size; j++) {
                     FunctionParameter parameter = parameters.get(j);
                     String parameterName = parameter.getName();

                     if(parameterName.equals("ref")) {
                        String componentName = parameter.getValue();
                        Object value = container.lookup(componentName);

                        if(!types[j].isInstance(value)) {
                           signatureMatch = false;
                           break;
                        }
                     }
                  }
                  if(signatureMatch) {
                     if(!methods[i].isAccessible()) {
                        methods[i].setAccessible(true);
                     }
                     methodCache.put(key, methods[i]);
                     return methods[i];
                  }
               }
            }
         }
         type = type.getSuperclass();
      }
      throw new IllegalArgumentException("Function " + key + " could not be found");
   }

   private String createMethodKey(String name, Class type, List<FunctionParameter> parameters) {
      return String.format("%s.%s(%s)", type.getName(), name, parameters);
   }
}
