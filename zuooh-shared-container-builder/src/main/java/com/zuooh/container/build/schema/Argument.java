package com.zuooh.container.build.schema;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.core.Validate;

import com.zuooh.common.reflect.PropertyMapper;

@Root
public class Argument {

   @Attribute(required=false)
   protected String ref;

   @Attribute(required=false)
   protected String value;

   @Element(required=false)
   protected Bean bean;

   @Element(required=false)
   protected MapComponent map;

   @Element(required=false)
   protected ListComponent list;

   @Element(required=false)
   protected SetComponent set;
   
   @Element(required=false)
   protected PropertiesComponent props;   

   @Validate
   private void validate() {
      String[] names = {"value", "ref", "bean", "list", "map", "set", "props"};
      Object[] values = {value, ref, bean, list, map, set, props};
      String previous = null;

      for(int i = 0; i < names.length; i++) {
         if(values[i] != null) {
            if(previous != null) {
               throw new IllegalStateException("Cannot declare '" +previous+ "' with '"+names[i]+"' in an argument");
            }
            previous = names[i];
         }
      }
   }

   public String getVariable(ContainerContext registry, Class type) throws Exception {
      if(ref != null) {
         ScopedComponent component = registry.componentFor(ref);

         if(component == null) {
            return PropertyMapper.getPropertyName(ref);
         }
         return component.getResultVariableName();
      }
      if(bean != null) {
         return bean.getResultVariableName();
      }
      if(map != null) {
         return map.getVariableName();
      }
      if(list != null) {
         return list.getVariableName();
      }
      if(set != null) {
         return set.getVariableName();
      }
      if(props != null) {
         return props.getVariableName();
      }
      if(value != null) {
         if(type == String.class) {
            return String.format("\"%s\"", value);
         }
         if(type == Class.class) {
            return String.format("%s.class", value);
         }
      }
      return value;
   }

   public Object getValue(ContainerContext context, Class[] dependents) throws Exception {
      if(ref != null) {
         if(context.isMap(ref)) {
            return context.lookupMap(ref, dependents);
         } else if(context.isSet(ref)) {
            return context.lookupSet(ref, dependents);
         } else if(context.isList(ref)) {
            return context.lookupList(ref, dependents);
         } else if(context.isProperties(ref)) {
            return context.lookupProperties(ref);
         }
         return context.lookupComponent(ref);
      }
      if(bean != null) {
         Object instance = bean.getInstance(context);
         bean.doProcessProperties(instance, context);
         return instance;
      }
      if(map != null) {
         return map.getValue(context, dependents);
      }
      if(list != null) {
         return list.getValue(context, dependents);
      }
      if(set != null) {
         return set.getValue(context, dependents);
      }
      if(props != null) {
         return props.getValue(context, dependents);
      }      
      return value;
   }

   public Class getType(ContainerContext context) throws Exception {
      if(ref != null) {
         if(context.isMap(ref)) {
            return Map.class;
         } else if(context.isSet(ref)) {
            return Set.class;
         } else if(context.isList(ref)) {
            return List.class;
         } else if(context.isProperties(ref)) {
            return Properties.class;
         }
         Object value = context.lookupComponent(ref);
         return value.getClass();
      }
      if(bean != null) {
         return bean.getType();
      }
      if(map != null) {
         return Map.class;
      }
      if(list != null) {
         return List.class;
      }
      if(set != null) {
         return Set.class;
      }
      if(props != null) {
         return Properties.class;
      }      
      return null;
   }

   @Override
   public String toString() {
      return ref;
   }
}
