package com.zuooh.container.build.schema;

import org.simpleframework.xml.Root;

@Root
public class CollectionBean extends Bean implements CollectionElement {

   @Override
   public Object getValue(ContainerContext context, Class type) throws Exception {
      if(cache == null) {
         Object instance = getInstance(context);
         doProcessProperties(instance, context);
         return instance;
      }
      return cache;
   }
   
   @Override
   public String getVariableName(ContainerContext container, Class type) throws Exception {
      return getResultVariableName();
   }
}
