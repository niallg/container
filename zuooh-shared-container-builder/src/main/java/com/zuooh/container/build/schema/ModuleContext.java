package com.zuooh.container.build.schema;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.zuooh.container.ContainerTracer;

public class ModuleContext implements ContainerContext {

   private final Set<String> dependencyOrder;
   private final CycleDetecter detecter;
   private final ModuleContainer container;
   private final FunctionExtractor extractor;

   public ModuleContext(ModuleContainer container, FunctionExtractor extractor) {
      this.dependencyOrder = new CopyOnWriteArraySet<String>();
      this.detecter = new CycleDetecter();
      this.container = container;
      this.extractor = extractor;
   }

   public Set<String> lookupOrder() {
      return Collections.unmodifiableSet(dependencyOrder);
   }

   @Override
   public boolean isComponent(String name) {
      return container.resolveComponent(name) != null;
   }
   
   @Override
   public boolean isProperties(String name) {
      return container.resolveProperties(name) != null;
   }

   @Override
   public boolean isMap(String name) {
      return container.resolveMap(name) != null;
   }

   @Override
   public boolean isList(String name) {
      return container.resolveList(name) != null;
   }

   @Override
   public boolean isSet(String name) {
      return container.resolveSet(name) != null;
   }

   @Override
   public Object lookupComponent(String name) throws Exception {
      try {
         detecter.pushToPath(name);
         return container.lookup(name);
      } finally {
         dependencyOrder.add(name);
         detecter.popFromPath(name);
      }
   }
   
   @Override
   public Properties lookupProperties(String name) throws Exception {
      PropertiesComponent component = container.resolveProperties(name);

      if(component == null) {
         throw new IllegalStateException("Could not find map of id '"+name+"'");
      }
      return component.getValue(this);
   }

   @Override
   public Map<Object, Object> lookupMap(String name, Class[] dependents) throws Exception {
      MapComponent component = container.resolveMap(name);

      if(component == null) {
         throw new IllegalStateException("Could not find map of id '"+name+"'");
      }
      return component.getValue(this, dependents);
   }

   @Override
   public Set<Object> lookupSet(String name, Class[] dependents) throws Exception {
      SetComponent component = container.resolveSet(name);

      if(component == null) {
         throw new IllegalStateException("Could not find set of id '"+name+"'");
      }
      return component.getValue(this, dependents);
   }

   @Override
   public List<Object> lookupList(String name, Class[] dependents) throws Exception {
      ListComponent component = container.resolveList(name);

      if(component == null) {
         throw new IllegalStateException("Could not find list of id '"+name+"'");
      }
      return component.getValue(this, dependents);
   }

   @Override
   public FunctionCall lookupFunction(String function, Class type) throws Exception {
      FunctionCall call = extractor.extractFunction(function, type);

      if(call == null) {
         throw new IllegalStateException("Could not find function '"+function+"'");
      }
      return call;
   }

   @Override
   public ScopedComponent componentFor(String name) {
      ScopedComponent component = container.resolveComponent(name);

      if(component == null) {
         component = container.resolveMap(name);
      }
      if(component == null) {
         component = container.resolveList(name);
      }
      if(component == null) {
         component = container.resolveSet(name);
      }
      return component;
   }

   @Override
   public ContainerTracer lookupTracer() {
      return container.getTracer();
   }
}
