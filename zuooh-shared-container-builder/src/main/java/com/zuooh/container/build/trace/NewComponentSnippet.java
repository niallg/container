package com.zuooh.container.build.trace;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zuooh.common.reflect.PropertyMapper;

public class NewComponentSnippet implements ContainerSnippet {

   private final List<String> arguments;
   private final Constructor factory;
   private final String id;

   public NewComponentSnippet(String id, Constructor factory, List<String> arguments) {
      this.id = id;
      this.factory = factory;
      this.arguments = arguments;
   }


   @Override
   public void assignments(Map<String, Class> typeInformationOnVariables) throws Exception {
      Class[] parameterTypes = factory.getParameterTypes();

      for(int i = 0; i < parameterTypes.length; i++) {
         String variable = arguments.get(i);
         Class parameterType = parameterTypes[i];

         if(parameterType != String.class && parameterType != Class.class) {
            Class existingType = typeInformationOnVariables.get(variable);

            if(existingType != null) {
               if(existingType.isAssignableFrom(parameterType)) {
                  typeInformationOnVariables.put(variable, parameterType);
               }
            } else {
               typeInformationOnVariables.put(variable, parameterType);
            }
         }
      }
   }


   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      Class type = factory.getDeclaringClass();
      String className = type.getName();
      String objectName = type.getSimpleName();
      Class[] parameterTypes = factory.getParameterTypes();

      for(Class parameterType : parameterTypes) {
         Class parentType = parameterType.getSuperclass();

         if(parentType != null && parentType.isEnum()) {
            String enumClassName = parentType.getName();
            importList.add("static " + enumClassName + ".*");
         } else if(parameterType.isEnum()) {
            String enumClassName = parameterType.getName();
            importList.add("static " + enumClassName + ".*");
         }
      }
      importList.add(className);

      StringBuilder text = new StringBuilder();

      builder.append("      final ");
      text.append(objectName);
      text.append(" ");
      text.append(PropertyMapper.getPropertyName(id));
      text.append(" = new ");
      text.append(objectName);
      text.append("(");

      if(arguments != null) {
         String prefix = "";

         for(String argument : arguments) {
            text.append(prefix);
            text.append(argument);
            prefix = ", ";
         }
      }
      text.append(");");
      text.append("\n");
      builder.append(text);
   }

   private void renderParameters(Appendable builder) throws IOException {
      Class[] parameterTypes = factory.getParameterTypes();
      String prefix = "";

      for(int i = 0; i < parameterTypes.length; i++) {
         Class parameterType = parameterTypes[i];
         String argument = arguments.get(i);

         builder.append(prefix);

         if(parameterType == String.class) {
            if(!argument.startsWith("\"")) {
               builder.append("\"");
            }
            builder.append(argument);

            if(!argument.endsWith("\"")) {
               builder.append("\"");
            }
         } else if(parameterType == Class.class) {
            builder.append(argument);
            builder.append(".class");
         } else {
            builder.append(argument);
         }
         prefix = ", ";
      }
   }
}
