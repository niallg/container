package com.zuooh.container.build.schema;

public class ComponentValue implements ScopedComponent {

   private final Component component;
   private final Object factory;
   private final Object value;

   public ComponentValue(Component component, Object value) {
      this(component, value, null);
   }

   public ComponentValue(Component component, Object value, Object factory) {
      this.component = component;
      this.factory = factory;
      this.value = value;
   }

   public Component getComponent() {
      return component;
   }

   public Object getInstance() {
      if(factory == null) {
         return value;
      }
      return factory;
   }

   public Object getValue() {
      return value;
   }

   @Override
   public String getName() {
      return component.getName();
   }

   @Override
   public String getResultVariableName() {
      return component.getResultVariableName();
   }

   @Override
   public String getVariableName() {
      return component.getVariableName();
   }

   @Override
   public Scope getScope() {
      return component.getScope();
   }
}
