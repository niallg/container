package com.zuooh.container.build;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Properties;
import java.util.Set;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.filter.Filter;
import org.simpleframework.xml.filter.MapFilter;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.FileSystem;
import com.zuooh.container.Container;
import com.zuooh.container.ContainerTracer;
import com.zuooh.container.build.schema.ModuleContainer;

public class FileSystemContainer implements Container {
   
   private static final Logger LOG = LoggerFactory.getLogger(FileSystemContainer.class);

   private final ModuleContainer container;
   private final FileSystem fileSystem;
   private final Serializer serializer;
   private final Strategy strategy;
   private final Filter filter;

   public FileSystemContainer(FileSystem fileSystem, Properties properties) {
      this.strategy = new TreeStrategy("type", "length");
      this.filter = new MapFilter(properties);
      this.container = new ModuleContainer();
      this.serializer = new Persister(strategy, filter);
      this.fileSystem = fileSystem;
   }

   public synchronized void loadFrom(String configFile) {
      long startTime = System.currentTimeMillis();

      try {
         InputStream source = fileSystem.openFile(configFile);

         container.registerSource(configFile);
         serializer.read(container, source);
         container.importAll(fileSystem, serializer);
      } catch(Exception e) {
         throw new IllegalStateException("Could not load container from " + configFile, e);
      } finally {
         long finishTime = System.currentTimeMillis();
         long loadTime = finishTime - startTime;
         
         LOG.info("Time taken to load "  + configFile + " was " + loadTime);
      }
   }

   @Override
   public synchronized void startAll() {
      container.startAll();
   }

   @Override
   public synchronized void stopAll() {
      container.stopAll();
   }

   @Override
   public synchronized void loadAll() {
      container.loadAll();
   }

   @Override
   public synchronized void register(String name, Object value) {
      container.register(name, value);
   }

   @Override
   public synchronized boolean contains(String name) {
      return container.contains(name);
   }

   @Override
   public synchronized <T> T lookup(Class<T> type) {
      return container.lookup(type);
   }   
   
   @Override
   public synchronized <T> Set<T> lookupAll(Class<T> type) {
      return container.lookupAll(type);
   }   

   @Override
   public synchronized <T> T lookup(String name) {
      return container.lookup(name);
   }

   @Override
   public synchronized ContainerTracer getTracer() {
      return container.getTracer();
   }

   @Override
   public synchronized Set<String> listAll() {
      return container.listAll();
   }

   @Override
   public synchronized Set<String> listMatches(Class<? extends Annotation> annotation) {
      return container.listMatches(annotation);
   }
}
