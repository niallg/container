package com.zuooh.container.build.schema;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


import com.zuooh.common.function.FunctionParameter;
import com.zuooh.common.reflect.Introspector;
import com.zuooh.common.util.StringConverter;
import com.zuooh.container.ContainerTracer;

public class FunctionCall {

   private final List<FunctionParameter> parameters;
   private final StringConverter converter;
   private final Method method;

   public FunctionCall(List<FunctionParameter> parameters, Method method) {
      this.converter = new StringConverter();
      this.parameters = parameters;
      this.method = method;
   }

   public Method getMethod() {
      return method;
   }

   public List<FunctionParameter> getParameters() {
      return Collections.unmodifiableList(parameters);
   }

   public Object call(String variableId, String assignToVariableId, ContainerContext context, Object object) throws Exception {
      ContainerTracer tracer = context.lookupTracer();
      Class[] parameterTypes = method.getParameterTypes();
      int size = parameters.size();

      if(size > 0) {
         List<String> variables = new LinkedList<String>();
         Object[] values = new Object[size];

         for(int i = 0; i < size; i++) {
            FunctionParameter parameter = parameters.get(i);
            Class[] dependents = Introspector.getParameterDependents(method, i);
            String name = parameter.getName();
            String value = parameter.getValue();

            if(name.equals("ref")) {
               ScopedComponent component = context.componentFor(value);
               String componentId = component.getResultVariableName();
               
               if(context.isMap(value)) {
                  values[i] = context.lookupMap(value, dependents);
               } else if(context.isSet(value)) {
                  values[i] = context.lookupSet(value, dependents);
               } else if(context.isList(value)) {
                  values[i] = context.lookupList(value, dependents);
               } else {
                  values[i] = context.lookupComponent(value);
               }
               variables.add(componentId);
            } else if(name.equals("value")){
               if(converter.accept(parameterTypes[i])) {
                  values[i] = converter.convert(parameterTypes[i], String.valueOf(value));
               }
               if(parameterTypes[i] == String.class) {
                  variables.add("\"" + value + "\"");
               } else {
                  variables.add(value);
               }
            } else {
               throw new IllegalStateException("Do not know how to process function argument of type " + name);
            }
         }
         tracer.callMethod(variableId, assignToVariableId, method, variables);
         return method.invoke(object, values);
      }
      tracer.callMethod(variableId, assignToVariableId, method, Collections.EMPTY_LIST);
      return method.invoke(object);
   }

   @Override
   public String toString() {
      return method.toString();
   }
}
