package com.zuooh.container.build.trace;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.reflect.PropertyMapper;
import com.zuooh.container.BasicContainer;
import com.zuooh.container.ContainerTracer;

public class ContainerGenerator implements ContainerTracer {
   
   private static final Logger LOG = LoggerFactory.getLogger(ContainerGenerator.class);

   private static final String TIMESTAMP_FIELD = "TIMESTAMP";
   private static final String SOURCE_FILES_FIELD = "FILES";
   private static final String DATE_CREATED_FIELD = "DATE_CREATED";

   private final List<ContainerSnippet> tracedElements;
   private final Map<String, Class> constructor;
   private final Map<String, Class> parameters;

   public ContainerGenerator() {
      this.tracedElements = new CopyOnWriteArrayList<ContainerSnippet>();
      this.constructor = new ConcurrentHashMap<String, Class>();
      this.parameters = new ConcurrentHashMap<String, Class>();
   }

   public void addParameter(String name, Class type) {
      parameters.put(name, type);
   }

   public void addConstructorParameter(String name, Class type) {
      constructor.put(name, type);
   }

   public void newInstanceCreated(String id, Constructor factory, List<String> variables) {
      NewComponentSnippet trace = new NewComponentSnippet(id, factory, variables);
      tracedElements.add(trace);
   }
   
   public void newPropertiesCreated(String id, String extendId) {
      NewCollectionSnippet trace = new NewCollectionSnippet(id, extendId, null, Properties.class);
      tracedElements.add(trace);
   }

   public void newMapCreated(String id, String extendId, Class[] dependents) {
      NewCollectionSnippet trace = new NewCollectionSnippet(id, extendId, dependents, LinkedHashMap.class);
      tracedElements.add(trace);
   }

   public void newListCreated(String id, String extendId, Class[] dependents) {
      NewCollectionSnippet trace = new NewCollectionSnippet(id, extendId, dependents, LinkedList.class);
      tracedElements.add(trace);
   }

   public void newSetCreated(String id, String extendId, Class[] dependents) {
      NewCollectionSnippet trace = new NewCollectionSnippet(id, extendId, dependents, LinkedHashSet.class);
      tracedElements.add(trace);
   }

   @Override
   public void callMethod(String id, String assignToId, Method method, List<String> variables) {
      MethodCallSnippet trace = new MethodCallSnippet(id, assignToId, method, variables);
      tracedElements.add(trace);
   }
   
   public void addEntryToProperties(String id, String key, String variable) {
      AddToPropertiesSnippet trace = new AddToPropertiesSnippet(id, key, variable);
      tracedElements.add(trace);
   }

   public void addEntryToMap(String id, String key, String variable) {
      AddToMapSnippet trace = new AddToMapSnippet(id, key, variable);
      tracedElements.add(trace);
   }

   public void addEntryToCollection(String id, String variable) {
      AddToCollectionSnippet trace = new AddToCollectionSnippet(id, variable);
      tracedElements.add(trace);
   }

   @Override
   public void addInclude(String fileName) {
      AddInclude trace = new AddInclude(fileName);
      tracedElements.add(trace);
   }

   public void registerComponent(String name, String id) {
      RegisterComponentSnippet trace = new RegisterComponentSnippet(name,  id);
      tracedElements.add(trace);
   }

   public void registerStopMethod(String id, Method method, List<String> variables) {
      RegisterStopMethodSnippet trace = new RegisterStopMethodSnippet(id, method, variables);
      tracedElements.add(trace);
   }

   public long lastModified(Class className) throws Exception {
      return lastModified(className.getName());
   }

   public static long lastModified(String fullClassName) throws Exception {
      try {
         Class generatedClass = Class.forName(fullClassName);
         Field timeStampField = generatedClass.getField(TIMESTAMP_FIELD);

         return timeStampField.getLong(null);
      } catch(Exception e) {
         LOG.info("Could not determine modification time for " + fullClassName);
      }
      return -1;
   }

   public static List<String> sourceFiles(Class className) throws Exception {
      return sourceFiles(className.getName());
   }

   public static List<String> sourceFiles(String fullClassName) throws Exception {
      try {
         Class generatedClass = Class.forName(fullClassName);
         Field sourceFilesField = generatedClass.getField(SOURCE_FILES_FIELD);
         String[] declaredFiles = (String[])sourceFilesField.get(null);

         return Arrays.asList(declaredFiles);
      } catch(Exception e) {
         LOG.info("Could not determine modification time for " + fullClassName);
      }
      return Collections.emptyList();
   }

   public void generateContainerSource(String sourceFile, String fullClassName, Class implementingInterface, String... outputPaths) {
      String implementingInterfaceObjectName = implementingInterface.getSimpleName();
      String implementingInterfaceName = implementingInterface.getName();
      int lastDotInPackage = fullClassName.lastIndexOf(".");
      String containerPackageName = fullClassName.substring(0, lastDotInPackage);
      String containerClassName = fullClassName.substring(lastDotInPackage + 1);
      File[] outputFiles = determineOutputs(fullClassName, outputPaths);
      String sourceCode = generateContainerSource(sourceFile, containerClassName, containerPackageName, implementingInterfaceName, implementingInterfaceObjectName);

      for(File outputFile : outputFiles) {
         try {
            FileWriter writer = new FileWriter(outputFile);

            writer.write(sourceCode);
            writer.flush();
            writer.close();
         } catch(Exception e){
            throw new IllegalStateException("Could not save source to " + outputFile, e);
         }
      }
   }

   private String generateContainerSource(String sourceFile, String containerClassName, String containerPackageName, String implementingInterfaceName, String implementingInterfaceObjectName) {
      try {
         Date dateOfGeneration = new Date();
         DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
         StringBuilder builder = new StringBuilder();
         TreeSet<String> importList = new TreeSet<String>();
         Set<String> importFiles = new HashSet<String>();
         Map<String, String> sessionState = new LinkedHashMap<String, String>();
         Map<String, Class> typeInformationOnVariables = new LinkedHashMap<String, Class>();
         StringBuilder collector = new StringBuilder();

         for(ContainerSnippet element : tracedElements) {
            element.assignments(typeInformationOnVariables);
         }
         for(ContainerSnippet element : tracedElements) {
            element.render(importList, importFiles, collector, sessionState, typeInformationOnVariables);
         }
         builder.append("package ");
         builder.append(containerPackageName);
         builder.append(";\n\n");

         Collection<Class> constructorTypes = constructor.values();

         for(Class typeToImport : constructorTypes) {
            String className = typeToImport.getName();
            importList.add(className);
         }
         Collection<Class> parameterTypes = parameters.values();

         for(Class typeToImport : parameterTypes) {
            String className = typeToImport.getName();
            importList.add(className);
         }
         String baseClass = BasicContainer.class.getName();
         String loggerClass = Logger.class.getName();
         String loggerFactoryClass = LoggerFactory.class.getName();

         importList.add(implementingInterfaceName);
         importList.add(loggerClass);
         importList.add(loggerFactoryClass);
         importList.add(baseClass);

         for(String importClass : importList) {
            builder.append("import ");
            builder.append(importClass);
            builder.append(";\n");
         }
         builder.append("\n");
         builder.append("public class ");
         builder.append(containerClassName);
         builder.append(" extends BasicContainer implements ");
         builder.append(implementingInterfaceObjectName);
         builder.append(" {\n");
         builder.append("\n");

         long timeOfGeneratingThisCode = System.currentTimeMillis();
         
         builder.append("   public static final Logger LOG = LoggerFactory.getLogger(");
         builder.append(containerClassName);
         builder.append(".class);\n\n");
         builder.append("   public static final long ");
         builder.append(TIMESTAMP_FIELD);
         builder.append(" = ");
         builder.append(String.valueOf(timeOfGeneratingThisCode));
         builder.append("L;\n");
         builder.append("   public static final String ");
         builder.append(DATE_CREATED_FIELD);
         builder.append(" = \"");
         builder.append(dateFormat.format(dateOfGeneration));
         builder.append("\";\n");            
         builder.append("   public static final String[] ");
         builder.append(SOURCE_FILES_FIELD);
         builder.append(" = {\n      \"");
         builder.append(sourceFile.replaceAll("\\\\","\\\\\\\\"));
         builder.append("\"");

         for(String importFile : importFiles) {
            builder.append(",\n      ");
            builder.append("\"");
            builder.append(importFile.replaceAll("\\\\","\\\\\\\\"));
            builder.append("\"");
         }
         builder.append("   };\n\n");

         generateDeclarations(builder);
         generateContainerConstructor(containerClassName, builder);
         generateParameters(builder);

         builder.append("\n");
         builder.append("   @Override\n");
         builder.append("   public <T> T lookup(String name) {\n");
         builder.append("      return lookup(name, 10000);\n");
         builder.append("   }\n");
         builder.append("   \n");
         builder.append("   @Override\n");
         builder.append("   public <T> T lookup(Class<T> type) {\n");
         builder.append("      return lookup(type, 10000);\n");
         builder.append("   }\n");
         builder.append("   \n");
         builder.append("   @Override\n");
         builder.append("   public void loadAll() {\n");
         builder.append("      try {\n");
         builder.append("         loadAndStartAll();\n");
         builder.append("      } catch(Exception e) {\n");
         builder.append("         LOG.info(\"Could not load all components\", e);\n");
         builder.append("      }\n");
         builder.append("   }\n");
         builder.append("\n");
         builder.append("   private void loadAndStartAll() throws Exception {\n");
         builder.append(collector);
         builder.append("\n");
         builder.append("   }\n");
         builder.append("}\n");
         return builder.toString();
      } catch(Exception e) {
         throw new IllegalStateException("Could not generate container for " + containerPackageName + "." + containerClassName, e);
      }
   }

   private void generateDeclarations(Appendable builder) throws IOException {
      Set<String> privateFinals = constructor.keySet();

      for(String name : privateFinals) {
         Class type = constructor.get(name);
         String objectName = type.getSimpleName();

         if(!parameters.containsKey(name)) {
            builder.append("   private final ");
            builder.append(objectName);
            builder.append(" ");
            builder.append(PropertyMapper.getPropertyName(name));
            builder.append(";\n");
         }
      }
      Set<String> privateVariables = parameters.keySet();

      for(String name : privateVariables) {
         Class type = parameters.get(name);
         String objectName = type.getSimpleName();

         builder.append("   private ");
         builder.append(objectName);
         builder.append(" ");
         builder.append(PropertyMapper.getPropertyName(name));
         builder.append(";\n");
      }
   }

   private void generateContainerConstructor(String containerClassName, Appendable builder) throws IOException {
      Set<String> names = constructor.keySet();

      builder.append("\n");
      builder.append("   public ");
      builder.append(containerClassName);
      builder.append("(");
      String prefix = "";

      for(String name : names) {
         Class type = constructor.get(name);
         String objectName = type.getSimpleName();

         builder.append(prefix);
         builder.append(objectName);
         builder.append(" ");
         builder.append(PropertyMapper.getPropertyName(name));
         prefix = ", ";
      }
      builder.append(") {\n");

      if(!names.isEmpty()) {
         for(String name : names) {
            builder.append("      this.");
            builder.append(PropertyMapper.getPropertyName(name));
            builder.append(" = ");
            builder.append(PropertyMapper.getPropertyName(name));
            builder.append(";\n");
         }
         for(String name : names) {
            builder.append("      register(\"");
            builder.append(name);
            builder.append("\", ");
            builder.append(PropertyMapper.getPropertyName(name));
            builder.append(");\n");
         }
      } else {
         builder.append("      super();\n");
      }
      builder.append("   }\n");
   }

   private void generateParameters(Appendable builder) throws IOException {
      Set<String> names = parameters.keySet();

      for(String name : names) {
         char firstChar = name.charAt(0);
         String remainingName = name.substring(1);
         String setterName = String.format("set%s%s", Character.toUpperCase(firstChar), remainingName);
         Class type = parameters.get(name);
         String parameterObjectName = type.getSimpleName();

         builder.append("\n");
         builder.append("   public void ");
         builder.append(setterName);
         builder.append("(");
         builder.append(parameterObjectName);
         builder.append(" ");
         builder.append(PropertyMapper.getPropertyName(name));
         builder.append(") {\n");
         builder.append("      this.");
         builder.append(PropertyMapper.getPropertyName(name));
         builder.append(" = ");
         builder.append(PropertyMapper.getPropertyName(name));
         builder.append(";\n");
         builder.append("      register(\"");
         builder.append(name);
         builder.append("\", ");
         builder.append(PropertyMapper.getPropertyName(name));
         builder.append(");\n");
         builder.append("   }\n");
      }
   }

   private File[] determineOutputs(String fullClassName, String[] outputPaths) {
      File[] resultingFiles = new File[outputPaths.length];
      int endOfPackage = fullClassName.lastIndexOf(".");
      String resultingPackage = fullClassName.substring(0, endOfPackage);
      String resultingClassName = fullClassName.substring(endOfPackage + 1);
      String packagePath = resultingPackage.replaceAll("\\.", "/");

      for(int i = 0; i < outputPaths.length; i++) {
         File resultingFolder = new File(outputPaths[i], packagePath);

         if(!resultingFolder.exists()) {
            resultingFolder.mkdir();
         }
         resultingFiles[i] = new File(resultingFolder, resultingClassName + ".java");
      }
      return resultingFiles;
   }
}
