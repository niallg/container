package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public interface ContainerSnippet {
   void assignments(Map<String, Class> typeInformationOnVariables) throws Exception;
   void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> sessionState, Map<String, Class> typeInformationOnVariables) throws Exception;
}
