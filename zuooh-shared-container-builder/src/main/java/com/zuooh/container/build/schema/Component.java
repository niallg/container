package com.zuooh.container.build.schema;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;
import org.simpleframework.xml.core.Validate;

import com.zuooh.common.reflect.PropertyMapper;

@Root(name="bean")
public class Component extends Bean implements ScopedComponent {

   @Attribute(name="id")
   private String id;

   @Attribute(name="init-method", required=false)
   private String start;

   @Attribute(name="destroy-method", required=false)
   private String stop;
   
   @Attribute(name="factory-bean", required=false)
   private String factory;

   @Attribute(name="factory-method", required=false)
   private String create;

   @Attribute(name="scope", required=false)
   private String scope;

   @Attribute(name="depends-on", required=false)
   private String[] requires;

   @Transient
   private FunctionCall createMethod;

   @Transient
   private FunctionCall stopMethod;

   @Transient
   private FunctionCall startMethod;
   
   public Component() {
      super();
   }
   
   @Validate
   public void validate() {
      if(type == null && factory == null) {
         throw new IllegalStateException("Bean must have type or factory-bean attribute");
      }
   }

   @Override
   public Scope getScope() {
      return Scope.resolveScope(scope);
   }

   @Override
   public String getName() {
      if(id == null) {
         throw new IllegalStateException("No id for component");
      }
      return id;
   }
   
   public String getFactory() {
      return factory;
   }
   
   @Override
   public String getVariableName() {
      String variableName = getResultVariableName();

      if(create != null) {
         return String.format("%sFactory", variableName);
      }
      return variableName;
   }

   @Override
   public String getResultVariableName() {      
      return PropertyMapper.getPropertyName(id) + System.identityHashCode(this);
   }

   public FunctionCall getStartMethod(ContainerContext registry) throws Exception {
      if(startMethod == null) {
         if(start != null) {
            startMethod = registry.lookupFunction(start, type);
         }
      }
      return startMethod;
   }

   public FunctionCall getStopMethod(ContainerContext registry) throws Exception {
      if(stopMethod == null) {
         if(stop != null) {
            stopMethod = registry.lookupFunction(stop, type);
         }
      }
      return stopMethod;
   }

   public FunctionCall getCreateMethod(ContainerContext registry) throws Exception {
      if(createMethod == null) {
         if(create != null) {
            if(factory != null) {
               Object value = registry.lookupComponent(factory);
               
               if(value == null) {
                  throw new IllegalStateException("Declared factory-bean '" + factory + "' could not be found");
               }
               Class factoryType = value.getClass();
               FunctionCall function = registry.lookupFunction(create, factoryType);
               
               if(function == null) {
                  throw new IllegalStateException("Declared factory-method '" + create + "' does not exist for " + factoryType);
               }
               createMethod = function;               
            } else {
               createMethod = registry.lookupFunction(create, type);
            }
         }
      }
      return createMethod;
   }
   
   @Override
   public Object getInstance(ContainerContext context) throws Exception {
      if(cache == null) {
         if(requires != null) {
            for(String requiredFirst : requires) {
               context.lookupComponent(requiredFirst);
            }
         }
         cache = super.getInstance(context);
      }
      return cache;

   }

   @Override
   public String toString() {
      return String.format("component '%s' of %s", id, type);
   }
}
