package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class NewCollectionSnippet extends BasicSnippet {

   private final Class collectionType;
   private final Class[] dependents;
   private final String extendId;
   private final String id;

   public NewCollectionSnippet(String id, String extendId, Class[] dependents, Class collectionType) {
      this.collectionType = collectionType;
      this.dependents = dependents;
      this.extendId = extendId;
      this.id = id;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      String alreadyDone = state.get(id);

      if(alreadyDone == null) {
         String fullPackageName = collectionType.getName();
         String constructorName = collectionType.getSimpleName();

         importList.add(fullPackageName);
         builder.append("      ");
         builder.append(constructorName);
         
         if(dependents != null && dependents.length > 0) {
            builder.append("<");
            String prefix = "";
   
            for(Class dependent : dependents) {
               String className = dependent.getName();
               String objectName = dependent.getSimpleName();
               Class parentType = dependent.getSuperclass();
   
               if(parentType != null && parentType.isEnum()) {
                  String enumClassName = parentType.getName();
                  importList.add("static " + enumClassName + ".*");
               } else if(dependent.isEnum()) {
                  String enumClassName = dependent.getName();
                  importList.add("static " + enumClassName + ".*");
               }
               importList.add(className);
               builder.append(prefix);
               builder.append(objectName);
               prefix = ",";
            }
            builder.append(">");
         }
         builder.append(" ");         
         builder.append(id);
         builder.append(" = new ");
         builder.append(constructorName);
         
         if(dependents != null && dependents.length > 0) {
            builder.append("<");
            String prefix = "";
   
            for(Class dependent : dependents) {
               String objectName = dependent.getSimpleName();
   
               builder.append(prefix);
               builder.append(objectName);
               prefix = ",";
            }
            builder.append(">");
         }
         builder.append("(");
         
         if(extendId != null) {
            builder.append(extendId);
         }
         builder.append(");\n");

         state.put(id, fullPackageName);
      }
   }
}
