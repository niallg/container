package com.zuooh.container.build.schema;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.Transient;

import com.zuooh.common.util.StringConverter;

@Root
public class CollectionValue implements CollectionElement {

   @Text
   private String value;   

   @Transient
   protected StringConverter converter;

   public CollectionValue() {
      this.converter = new StringConverter();
   }

   @Override
   public Object getValue(ContainerContext container, Class type) {    
      if(value != null) {
         if(type == Object.class) {
            return value;
         }
         return converter.convert(type, value);
      }
      return null;
   }

   @Override
   public String getVariableName(ContainerContext container, Class type) throws Exception {
      if(type == String.class) {
         return String.format("\"%s\"", value);
      }
      if(type == Object.class) {
         return String.format("\"%s\"", value); // untyped is treated as string
      }
      if(type == Class.class) {
         return String.format("%s.class", value);
      }
      return value;
   }
}
