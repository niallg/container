package com.zuooh.container.build.trace;

import java.util.Map;
import java.util.Set;

public class AddToMapSnippet extends BasicSnippet {

   private final String map;
   private final String key;
   private final String variable;
   private final String uniqueKey;

   public AddToMapSnippet(String map, String key, String variable) {
      this.uniqueKey = String.format("%s.%s", map, key);
      this.map = map;
      this.key = key;
      this.variable = variable;
   }

   @Override
   public void render(Set<String> importList, Set<String> sourceFiles, Appendable builder, Map<String, String> state, Map<String, Class> typeInformationOnVariables) throws Exception {
      String alreadyDone = state.get(uniqueKey);

      if(!variable.equals(alreadyDone)) {
         builder.append("      ");         
         builder.append(map);
         builder.append(".put(");
         builder.append(key);
         builder.append(", ");
         builder.append(variable);
         builder.append(");\n");
         state.put(uniqueKey, variable);
      }
   }
}
