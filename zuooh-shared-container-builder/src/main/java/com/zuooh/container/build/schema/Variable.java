package com.zuooh.container.build.schema;

import java.util.Map;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.core.Commit;
import org.simpleframework.xml.core.Validate;

@Root
public class Variable implements org.simpleframework.xml.util.Entry {
   
   @Attribute
   private String name;
   
   @Text
   private String value;

   @Validate
   public void validate(Map session) {
      if(session.containsKey(name)) {
         throw new IllegalStateException("Variable " + value + " already declared");
      }
   }
   
   @Commit
   public void commit(Map session) {
      session.put(name,  value);
   }

   @Override
   public String getName() {
      return name;
   }

}
