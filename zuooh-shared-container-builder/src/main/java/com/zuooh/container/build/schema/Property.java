package com.zuooh.container.build.schema;

import org.simpleframework.xml.Attribute;

public class Property extends Argument {

   @Attribute(name="name")
   private String name;

   public String getName() {
      return name;
   }

   @Override
   public String toString() {
      return String.format("%s: %s", name, getClass());
   }
}
