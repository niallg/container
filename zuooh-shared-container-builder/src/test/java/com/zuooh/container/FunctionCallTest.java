package com.zuooh.container;

import java.util.Map;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.container.build.schema.ModuleContainer;

public class FunctionCallTest extends TestCase {

   private static final String SOURCE =
   "<beans>\n"+
   "" +
   "   <map id='DoubleMap'>" +
   "      <entry key='1' value='12.0'/>\n"+
   "      <entry key='2' value='122.11'/>\n"+
   "      <entry key='3' value='-42.33'/>\n"+
   "   </map>\n" +
   ""+
   "   <map id='IntMap'>" +
   "      <entry key='1' value='12'/>\n"+
   "      <entry key='2' value='11'/>\n"+
   "      <entry key='3' value='10'/>\n"+
   "   </map>\n" +
   "" +
   "   <bean id='X' class='com.zuooh.container.FunctionCallTest$X' factory-method='make(ref: IntMap)'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Y' class='com.zuooh.container.FunctionCallTest$Y'>\n"+
   "      <constructor-arg ref='X'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Z' class='com.zuooh.container.FunctionCallTest$Z' init-method=\"start(value: 'start me up scotty')\">\n"+
   "      <constructor-arg ref='X'/>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "</beans>\n";

   private static class X {

      private Map<String, Double> doubleMap;

      public X(Map<String, Double> doubleMap) {
         this.doubleMap = doubleMap;
      }

      public Y make(Map<String, Integer> intMap) {
         return new Y(doubleMap, intMap);
      }
   }

   private static class Y {

      private Map<String, Double> doubleMap;
      private Map<String, Integer> intMap;

      public Y(Map<String, Double> doubleMap, Map<String, Integer> intMap) {
         this.doubleMap = doubleMap;
         this.intMap = intMap;
      }
   }

   private static class Z {

      private Map<String, String> stringMap;
      private Y y;
      private String startText;

      private Z(Y y, Map<String, String> stringMap) {
         this.y = y;
         this.stringMap = stringMap;
      }

      public void start(String startText) {
         this.startText = startText;
      }
   }

   public void testFunctionCall() throws Exception {
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      Container container = persister.read(ModuleContainer.class, SOURCE);
      Z z = container.lookup(Z.class);

      container.startAll();

      assertEquals(z.y.intMap.get("1"), new Integer(12));
      assertEquals(z.y.intMap.get("2"), new Integer(11));
      assertEquals(z.y.intMap.get("3"), new Integer(10));
      assertEquals(z.y.doubleMap.get("1"), new Double(12.0));
      assertEquals(z.y.doubleMap.get("2"), new Double(122.11));
      assertEquals(z.y.doubleMap.get("3"), new Double(-42.33));
      assertEquals(z.stringMap.get("1"), "12.0");
      assertEquals(z.stringMap.get("2"), "122.11");
      assertEquals(z.stringMap.get("3"), "-42.33");
      assertEquals(z.startText, "start me up scotty");
   }
}
