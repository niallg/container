package com.zuooh.container;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.TestCase;

import com.zuooh.common.FileSystem;
import com.zuooh.container.build.FileSystemContainer;

public class ModuleTest extends TestCase {

   private static final String TEXT =
   "<beans>\n"+
   "\n"+
   "   <map id='DoubleMap'>\n"+
   "      <entry key='1' value='12.0'/>\n"+
   "      <entry key='2' value='122.11'/>\n"+
   "      <entry key='3' value='-42.33'/>\n"+
   "   </map>\n"+
   "\n"+
   "   <list id='DateList'>\n"+
   "      <value>2011-01-01</value>\n"+
   "      <value>2012-12-30</value>\n"+
   "   </list>\n"+
   "\n"+
   "   <module name='X1'>\n"+
   "\n"+
   "      <map id='DoubleMap'>\n"+
   "         <entry key='1' value='11'/>\n"+
   "         <entry key='2' value='22'/>\n"+
   "         <entry key='3' value='33'/>\n"+
   "      </map>\n"+
   "\n"+
   "      <bean id='X' class='com.zuooh.container.ModuleTest$X'>\n"+
   "         <constructor-arg ref='DoubleMap'/>\n"+
   "      </bean>\n"+
   "\n"+
   "   </module>\n"+
   "\n"+
   "   <module name='X2'>\n"+
   "\n"+
   "      <map id='DoubleMap'>\n"+
   "         <entry key='1' value='1'/>\n"+
   "         <entry key='2' value='2'/>\n"+
   "         <entry key='3' value='3'/>\n"+
   "         <entry key='4' value='4'/>\n"+
   "         <entry key='5' value='5'/>\n"+
   "      </map>\n"+
   "\n"+
   "      <bean id='X' class='com.zuooh.container.ModuleTest$X' scope='private'>\n"+
   "         <constructor-arg ref='DoubleMap'/>\n"+
   "      </bean>\n"+
   "\n"+
   "      <bean id='Z2' class='com.zuooh.container.ModuleTest$Z' scope='public'>\n"+
   "         <constructor-arg ref='DoubleMap'/>\n"+
   "         <constructor-arg ref='DateList'/>\n"+
   "      </bean>\n"+
   "\n"+
   "   </module>\n"+
   "\n"+
   "   <bean id='Y' class='com.zuooh.container.ModuleTest$Y'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   "\n"+
   "   <bean id='Z' class='com.zuooh.container.ModuleTest$Z'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "      <constructor-arg ref='DateList'/>\n"+
   "   </bean>\n"+
   "\n"+
   "</beans>\n";


   private static class X {

      private Map<String, Double> doubleMap;

      public X(Map<String, Double> doubleMap) {
         this.doubleMap = doubleMap;
      }
   }

   private static class Y {

      private Map<String, String> stringMap;

      public Y(Map<String, String> stringMap) {
         this.stringMap = stringMap;
      }
   }

   private static class Z {

      private Map<String, Float> floatMap;
      private List<String> dates;

      public Z(Map<String, Float> floatMap, List<String> dates) {
         this.floatMap = floatMap;
         this.dates = dates;
      }
   }

   public void testModules() throws Exception {
      Map<String, String> files = new HashMap<String, String>();
      FileSystem fileSystem = new MapFileSystem(files);
      Properties properties = new Properties();
      FileSystemContainer container = new FileSystemContainer(fileSystem, properties);

      files.put("spring.xml", TEXT);
      container.loadFrom("spring.xml");

      X x = container.lookup("X");

      assertEquals(x.doubleMap.get("1"), 11.0);
      assertEquals(x.doubleMap.get("2"), 22.0);
      assertEquals(x.doubleMap.get("3"), 33.0);

      Z z = container.lookup("Z2");

      assertEquals(z.floatMap.get("1"), 1f);
      assertEquals(z.floatMap.get("2"), 2f);
      assertEquals(z.floatMap.get("3"), 3f);
      assertEquals(z.floatMap.get("4"), 4f);
      assertEquals(z.floatMap.get("5"), 5f);

      assertEquals(z.dates.get(0), "2011-01-01");
      assertEquals(z.dates.get(1), "2012-12-30");
   }

   public static class MapFileSystem implements FileSystem {

      private final Map<String, String> files;

      public MapFileSystem(Map<String, String> files) {
         this.files = files;
      }

      @Override
      public InputStream openFile(String file) {
         String content = files.get(file);

         try {
            byte[] bytes = content.getBytes("UTF-8");
            return new ByteArrayInputStream(bytes);
         } catch(Exception e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public OutputStream createExternalFile(String file) {
         return System.out;
      }

      @Override
      public InputStream openExternalFile(String file) {
         return openFile(file);
      }

      @Override
      public String[] listExternalFiles(String filter) {
         return null;
      }

      @Override
      public String[] listFiles(String filter) {
         return null;
      }

      @Override
      public boolean deleteExternalFile(String file) {
         return false;
      }

      @Override
      public boolean deleteFile(String file) {
         return false;
      }

      @Override
      public long lastModified(String file) {
         return 0;
      }

      @Override
      public long lastExternalModified(String file) {
         return 0;
      }

      @Override
      public long freeExternalSpace() {
         return 0;
      }

      @Override
      public long sizeOfExternalFile(String file) {
         return 0;
      }

      @Override
      public boolean touchExternalFile(String file) {
         return false;
      }

      @Override
      public byte[] loadFile(String file) {
         return null;
      }

      @Override
      public byte[] loadExternalFile(String file) {
         return null;
      }

      @Override
      public long sizeOfFile(String fileName) {
         return 0;
      }
   }
}
