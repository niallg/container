package com.zuooh.container;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import junit.framework.TestCase;

import com.zuooh.common.FileSystem;
import com.zuooh.container.build.FileSystemContainer;

public class ContainerCheckedEntryTest extends TestCase {

   private static final String TEXT =
   "<beans>\n"+
   "" +
   "   <bean id='X' class='com.zuooh.container.ContainerCheckedEntryTest$X'>\n"+
   "      <constructor-arg>\n"+
   "         <map>\n" +
   "            <entry key='Locale' value='java.util.Locale'/>\n"+
   "            <entry key='Date' value='java.util.Date'/>\n"+
   "            <entry key='Map' value='java.util.Map'/>\n"+
   "            <entry key='Set' value='java.util.Set'/>\n"+
   "            <entry key='List' value='java.util.List'/>\n"+
   "            <entry key='X' value='com.zuooh.container.ContainerCheckedEntryTest$X'/>" +
   "            <entry key='ContainerCheckedEntryTest' value='com.zuooh.container.ContainerCheckedEntryTest'/>" +
   "         </map>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg>\n"+
   "         <map>\n" +
   "            <entry key='1' value='12.3334'/>\n"+
   "            <entry key='2' value='10'/>\n"+
   "            <entry key='3' value='-100.334'/>\n"+
   "         </map>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg>\n"+
   "         <map>\n" +
   "            <entry key='start' value='2011-10-11'/>\n"+
   "            <entry key='end' value='2016-01-09'/>\n"+
   "         </map>\n" +
   "      </constructor-arg>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Y' class='com.zuooh.container.ContainerCheckedEntryTest$Y'>\n"+
   "      <constructor-arg>\n"+
   "         <set>\n" +
   "            <value>com.zuooh.container.ContainerCheckedEntryTest$Y</value>" +
   "            <value>com.zuooh.container.ContainerCheckedEntryTest$X</value>" +
   "            <value>com.zuooh.container.ContainerCheckedEntryTest</value>" +
   "         </set>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg>\n"+
   "         <list>\n" +
   "            <value>1.11</value>\n"+
   "            <value>-2.2210</value>\n"+
   "            <value>14</value>\n"+
   "         </list>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg>\n"+
   "         <set>\n" +
   "            <value>Example Text</value>\n"+
   "            <value>Other Example Text</value>\n"+
   "         </set>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg>\n"+
   "         <list>\n" +
   "            <value>B</value>\n"+
   "            <value>A</value>\n"+
   "            <value>C</value>\n"+
   "         </list>\n" +
   "      </constructor-arg>\n"+
   "   </bean>\n"+
   "" +
   "</beans>\n";

   private static class X {

      public final Map<String, Class> types;
      public final Map<String, Double> doubles;
      public final Map<String, String> dates;

      public X(Map<String, Class> types, Map<String, Double> doubles, Map<String, String> dates) {
         this.types = types;
         this.doubles = doubles;
         this.dates = dates;
      }
   }

   private static class Y {

      public final Set<Class> types;
      public final List<Float> floats;
      public final Set<String> strings;
      public final List<Z> enums;

      public Y(Set<Class> types, List<Float> floats, Set<String> strings, List<Z> enums) {
         this.types = types;
         this.floats = floats;
         this.strings = strings;
         this.enums = enums;
      }
   }

   private static enum Z {
      A,
      B,
      C
   }


   public void testContainer() throws Exception {
      Map<String, String> files = new HashMap<String, String>();
      FileSystem fileSystem = new MapFileSystem(files);
      Properties properties = new Properties();
      FileSystemContainer container = new FileSystemContainer(fileSystem, properties);

      files.put("spring.xml", TEXT);
      container.loadFrom("spring.xml");

      X x = container.lookup("X");
      Y y = container.lookup("Y");

      assertNotNull(x);
      assertNotNull(x.dates);
      assertNotNull(x.doubles);
      assertNotNull(x.types);
      assertEquals(x.types.get("Locale"), Locale.class);
      assertEquals(x.types.get("Date"), Date.class);
      assertEquals(x.types.get("Map"), Map.class);
      assertEquals(x.types.get("Set"), Set.class);
      assertEquals(x.types.get("List"), List.class);
      assertEquals(x.types.get("X"), X.class);
      assertEquals(x.types.get("ContainerCheckedEntryTest"), ContainerCheckedEntryTest.class);
      assertEquals(x.doubles.get("1"), 12.3334);
      assertEquals(x.doubles.get("2"), 10d);
      assertEquals(x.doubles.get("3"), -100.334);
      assertEquals(x.dates.get("start"), "2011-10-11");
      assertEquals(x.dates.get("end"), "2016-01-09");
      assertNotNull(y.types);
      assertNotNull(y.floats);
      assertNotNull(y.strings);
      assertNotNull(y.enums);
      assertTrue(y.types.contains(X.class));
      assertTrue(y.types.contains(Y.class));
      assertTrue(y.types.contains(ContainerCheckedEntryTest.class));
      assertEquals(y.floats.get(0), 1.11f);
      assertEquals(y.floats.get(1), -2.2210f);
      assertEquals(y.floats.get(2), 14f);
      assertTrue(y.strings.contains("Example Text"));
      assertTrue(y.strings.contains("Other Example Text"));
      assertEquals(y.enums.get(0), Z.B);
      assertEquals(y.enums.get(1), Z.A);
      assertEquals(y.enums.get(2), Z.C);

   }

   public static class MapFileSystem implements FileSystem {

      private final Map<String, String> files;

      public MapFileSystem(Map<String, String> files) {
         this.files = files;
      }

      @Override
      public InputStream openFile(String file) {
         String content = files.get(file);

         try {
            byte[] bytes = content.getBytes("UTF-8");
            return new ByteArrayInputStream(bytes);
         } catch(Exception e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public OutputStream createExternalFile(String file) {
         return System.out;
      }

      @Override
      public InputStream openExternalFile(String file) {
         return openFile(file);
      }

      @Override
      public String[] listExternalFiles(String filter) {
         return null;
      }

      @Override
      public String[] listFiles(String filter) {
         return null;
      }

      @Override
      public boolean deleteExternalFile(String file) {
         return false;
      }

      @Override
      public boolean deleteFile(String file) {
         return false;
      }

      @Override
      public long lastModified(String file) {
         return 0;
      }

      @Override
      public long lastExternalModified(String file) {
         return 0;
      }

      @Override
      public long freeExternalSpace() {
         return 0;
      }

      @Override
      public long sizeOfExternalFile(String file) {
         return 0;
      }

      @Override
      public boolean touchExternalFile(String file) {
         return false;
      }

      @Override
      public byte[] loadFile(String file) {
         return null;
      }

      @Override
      public byte[] loadExternalFile(String file) {
         return null;
      }

      @Override
      public long sizeOfFile(String fileName) {
         return 0;
      }
   }
}
