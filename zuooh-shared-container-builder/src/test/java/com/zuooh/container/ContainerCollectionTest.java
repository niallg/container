package com.zuooh.container;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.container.build.schema.ModuleContainer;

public class ContainerCollectionTest extends TestCase {

   private static final String TEXT =
   "<beans>\n"+
   "" +
   "   <map id='DoubleMap'>" +
   "      <entry key='1' value='12.0'/>\n"+
   "      <entry key='2' value='122.11'/>\n"+
   "      <entry key='3' value='-42.33'/>\n"+
   "   </map>\n" +
   "" +
   "   <list id='DateList'>" +
   "      <value>2011-01-01</value>\n"+
   "      <value>2012-12-30</value>\n"+
   "   </list>\n" +
   "" +
   "   <bean id='X' class='com.zuooh.container.ContainerCollectionTest$X'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Y' class='com.zuooh.container.ContainerCollectionTest$Y'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Z' class='com.zuooh.container.ContainerCollectionTest$Z'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "      <constructor-arg ref='DateList'/>\n"+
   "   </bean>\n"+
   ""+
   "</beans>\n";

   private static final String ERROR_MAP_HAS_NO_ID =
   "<beans>\n"+
   "" +
   "   <map>" +
   "      <entry key='1' value='12.0'/>\n"+
   "      <entry key='2' value='122.11'/>\n"+
   "      <entry key='3' value='-42.33'/>\n"+
   "   </map>\n" +
   "" +
   "   <bean id='X' class='com.zuooh.container.ContainerCollectionTest$X'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Y' class='com.zuooh.container.ContainerCollectionTest$Y'>\n"+
   "      <constructor-arg ref='DoubleMap'/>\n"+
   "   </bean>\n"+
   ""+
   "</beans>\n";

   private static class X {

      private Map<String, Double> doubleMap;

      public X(Map<String, Double> doubleMap) {
         this.doubleMap = doubleMap;
      }
   }

   private static class Y {

      private Map<String, String> stringMap;

      public Y(Map<String, String> stringMap) {
         this.stringMap = stringMap;
      }
   }

   private static class Z {

      private Map<String, Float> floatMap;
      private List<String> dates;

      public Z(Map<String, Float> floatMap, List<String> dates) {
         this.floatMap = floatMap;
         this.dates = dates;
      }
   }

   public void testContainer() throws Exception {
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      Container container = persister.read(ModuleContainer.class, TEXT);
      X x = container.lookup(X.class);
      Y y = container.lookup(Y.class);
      Z z = container.lookup(Z.class);

      assertNotNull(x);
      assertNotNull(y);
      assertNotNull(z);
      assertEquals(x.doubleMap.get("1"), 12.0);
      assertEquals(x.doubleMap.get("2"), 122.11);
      assertEquals(x.doubleMap.get("3"), -42.33);
      assertEquals(y.stringMap.get("1"), "12.0");
      assertEquals(y.stringMap.get("2"), "122.11");
      assertEquals(y.stringMap.get("3"), "-42.33");
      assertEquals(z.floatMap.get("1"), 12.0f);
      assertEquals(z.floatMap.get("2"), 122.11f);
      assertEquals(z.floatMap.get("3"), -42.33f);
      assertEquals(z.dates.get(0), "2011-01-01");
      assertEquals(z.dates.get(1), "2012-12-30");
   }

   public void testMapHasNoId() throws Exception {
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      boolean failureWithException = false;

      try {
         Container container = persister.read(ModuleContainer.class, ERROR_MAP_HAS_NO_ID);
         X x = container.lookup(X.class);
         Y y = container.lookup(Y.class);
      } catch(Exception e) {
         e.printStackTrace();
         failureWithException = true;
      }
      assertTrue("Map must be declared with an id", failureWithException);
   }
}
