package com.zuooh.container;

import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.container.build.schema.ModuleContainer;

public class ContainerTest extends TestCase {

   private static final String TEXT =
   "<beans>\n"+
   "" +
   "   <bean id='X' class='com.zuooh.container.ContainerTest$X' init-method='startMe' destroy-method='stopMe'>\n"+
   "      <constructor-arg value='Example Text'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Y' class='com.zuooh.container.ContainerTest$Y' init-method='startIt'>\n"+
   "      <constructor-arg ref='X'/>\n"+
   "      <constructor-arg value='2011-10-01'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Y2' class='com.zuooh.container.ContainerTest$Y' init-method='startIt'>\n"+
   "      <constructor-arg>\n"+
   "         <bean class='com.zuooh.container.ContainerTest$X'>\n"+
   "            <constructor-arg value='Inner Text'/>\n"+
   "         </bean>\n"+
   "      </constructor-arg>\n"+
   "      <constructor-arg value='2010-10-01'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Y3' class='com.zuooh.container.ContainerTest$Y'>\n"+
   "      <constructor-arg>\n"+
   "         <bean class='com.zuooh.container.ContainerTest$X'>\n"+
   "            <constructor-arg value='Text Y3'/>\n"+
   "         </bean>\n"+
   "      </constructor-arg>\n"+
   "      <constructor-arg value='2010-10-01'/>\n"+
   "      <property name='id' value='12'/>\n"+
   "      <property name='z' ref='Z3'/>\n"+
   "   </bean>\n"+
   ""+
   "   <bean id='Z' class='com.zuooh.container.ContainerTest$Z'>\n"+
   "      <constructor-arg>\n"+
   "         <map>\n" +
   "           <entry key='1' value-ref='X'/>\n"+
   "           <entry key='2' value-ref='Y'/>\n"+
   "           <entry key='3' value='Text Entry'/>\n"+
   "         </map>\n" +
   "      </constructor-arg>\n"+
   "      <constructor-arg ref='Y'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Z2' class='com.zuooh.container.ContainerTest$Z'>\n"+
   "      <constructor-arg>\n"+
   "         <list>\n" +
   "           <ref bean='X'/>\n"+
   "           <ref bean='Y'/>\n"+
   "           <value>Some Text</value>\n"+
   "           <ref bean='Z'/>\n"+              
   "           <bean class='com.zuooh.container.ContainerTest$X'>\n"+
   "              <constructor-arg value='Inner X'/>" +
   "           </bean>\n"+
   "         </list>\n" +
   "      </constructor-arg>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Z3' class='com.zuooh.container.ContainerTest$Z'>\n"+
   "      <constructor-arg>\n"+
   "         <list>\n" +
   "           <ref bean='Y'/>\n"+
   "           <value>Some Text</value>\n"+
   "           <ref bean='Z'/>\n"+
   "           <bean class='com.zuooh.container.ContainerTest$X'>\n"+
   "              <constructor-arg value='Inner X'/>" +
   "           </bean>\n"+
   "         </list>\n" +
   "      </constructor-arg>\n"+
   "   </bean>\n"+
   "" +
   "</beans>\n";

   private static class X {

      private String text;
      private boolean started;
      private boolean stopped;

      public X(String text) {
         this.text = text;
      }

      public void startMe() {
         started = true;
      }

      public void stopMe() {
         stopped = true;
      }
   }

   private static class Y {

      private String date;
      private X x;
      private boolean started;
      private Integer id;
      private Z z;

      public Y(X x, String date) {
         this.date = date;
         this.x = x;
      }

      public void startIt() {
         started = true;
      }

      public void setId(Integer id) {
         this.id = id;
      }

      public void setZ(Z z) {
         this.z = z;
      }
   }

   private static class Z {

      private Map<String, Object> map;
      private List<Object> list;
      private Y y;

      public Z(Map<String, Object> map, Y y) {
         this.list = list;
         this.map = map;
         this.y = y;
      }

      public Z(List<Object> list) {
         this.list = list;
      }
   }

   public void testContainer() throws Exception {
      long startTime = System.currentTimeMillis();
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      Container container = persister.read(ModuleContainer.class, TEXT);
      long parseDuration = System.currentTimeMillis() - startTime;
      System.err.println("Parse time: "+parseDuration);

      for(int i = 0; i < 10; i++) {
         startTime = System.currentTimeMillis();
         container = persister.read(ModuleContainer.class, TEXT);
         parseDuration = System.currentTimeMillis() - startTime;
         System.err.println("Parse time: "+parseDuration);
      }
      Y y3 = container.lookup("Y3");
      Z z = container.lookup("Z");
      X x = container.lookup("X");
      Y y = container.lookup("Y");
      Z z2 = container.lookup("Z2");
      Y y2 = container.lookup("Y2");
      Z z3 = container.lookup("Z3");
      long loadDuration = System.currentTimeMillis() - startTime;
      System.err.println("Load taken: "+loadDuration);

      container.startAll();

      assertEquals(x.text, "Example Text");
      assertTrue(x.started);
      assertFalse(x.stopped);
      assertEquals(y.x, x);
      assertTrue(y.started);
      assertNotNull(y.date);
      assertEquals(z.map.get("1"), x);
      assertEquals(z.map.get("2"), y);
      assertEquals(z.map.get("3"), "Text Entry");
      assertNull(z.list);
      assertEquals(z.y, y);
      assertEquals(z2.list.get(0), x);
      assertEquals(z2.list.get(1), y);
      assertEquals(z2.list.get(2), "Some Text");
      assertEquals(z2.list.get(3), z);
      assertNotNull(z2.list.get(4));
      assertEquals(((X)z2.list.get(4)).text, "Inner X");
      assertNull(z2.map);
      assertNull(z2.y);
      assertNotNull(y2.date);
      assertEquals(y2.x.text, "Inner Text");
      assertEquals(y3.z, z3);
      assertEquals(y3.id.intValue(), 12);
      assertEquals(y3.x.text, "Text Y3");

      container.startAll();

      assertTrue(x.started);
      assertTrue(y.started);

      container.stopAll();

      assertTrue(x.stopped);

   }
}
