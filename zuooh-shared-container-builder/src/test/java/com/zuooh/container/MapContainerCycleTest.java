package com.zuooh.container;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.container.build.schema.ModuleContainer;

public class MapContainerCycleTest extends TestCase {

   private static final String SOURCE =
   "<beans>\n"+
   "\n"+
   "  <bean id='X' class='com.zuooh.container.MapContainerCycleTest$X'>\n"+
   "    <constructor-arg ref='Y'/>\n"+
   "  </bean>\n"+
   "\n"+
   "  <bean id='Y' class='com.zuooh.container.MapContainerCycleTest$Y'>"+
   "    <constructor-arg ref='X'/>"+
   "  </bean>"+
   "\n"+
   "</beans>";

   public static class X{
      public X(Y y){}
   }

   public static class Y{
      public Y(X x){}
   }

   public void testCycle() throws Exception {
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      ModuleContainer container = persister.read(ModuleContainer.class, SOURCE);
      boolean failure = false;

      try {
         container.lookup("X");
      }catch(Exception e){
         e.printStackTrace();
         failure = true;
      }
      assertTrue("Cycle should be detected", failure);
   }
}
