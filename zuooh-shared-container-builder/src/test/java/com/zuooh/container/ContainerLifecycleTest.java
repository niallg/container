package com.zuooh.container;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import com.zuooh.container.build.schema.ModuleContainer;

public class ContainerLifecycleTest extends TestCase {


   private static final String TEXT =
   "<beans>\n"+
   "" +
   "   <bean id='Date' class='com.zuooh.container.ContainerLifecycleTest$DateFactory' factory-method='createDate'/>\n"+
   "" +
   "   <bean id='Service' class='com.zuooh.container.ContainerLifecycleTest$ServiceFactory' factory-method='createService' init-method='startService' destroy-method='stopService'>\n"+
   "      <constructor-arg value='LOW_PRIORITY'/>\n"+
   "      <constructor-arg ref='Date'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Widget1' class='com.zuooh.container.ContainerLifecycleTest$WidgetFactory' factory-method='createWidget'>\n"+
   "      <constructor-arg ref='Date'/>\n"+
   "   </bean>\n"+
   "" +
   "   <bean id='Widget2' class='com.zuooh.container.ContainerLifecycleTest$WidgetFactory' factory-method='createWidget'>\n"+
   "      <constructor-arg ref='Date'/>\n"+
   "   </bean>\n"+
   "" +
   "</beans>\n";

   private static class DateFactory {

      public Date createDate() {
         return new Date();
      }
   }

   private static class ServiceFactory {

      private final AtomicInteger createCount;
      private final Service service;

      public ServiceFactory(ServicePriority servicePriority, Date startDate) {
         this.createCount = new AtomicInteger(0);
         this.service = new Service(servicePriority, startDate);
      }

      public Service createService(){
         createCount.getAndIncrement();
         return service;
      }

      public void startService() {
         service.start();
      }

      public void stopService() {
         service.stop();
      }
   }

   private static class Service {

      private ServicePriority servicePriority;
      private Date startDate;
      private boolean started;
      private boolean stopped;

      public Service(ServicePriority servicePriority, Date startDate) {
         this.servicePriority = servicePriority;
         this.startDate = startDate;
      }

      public Date getDate() {
         return startDate;
      }

      public void start() {
         started = true;
      }

      public void stop() {
         stopped = true;
      }
   }

   private static class WidgetFactory {

      private final Date widgetDate;

      public WidgetFactory(Date widgetDate) {
         this.widgetDate = widgetDate;
      }

      public Widget createWidget() {
         return new Widget(widgetDate);
      }
   }

   private static class Widget {

      private final Date widgetDate;

      public Widget(Date widgetDate) {
         this.widgetDate = widgetDate;
      }
   }

   private static enum ServicePriority {
      HIGH_PRIORITY,
      NORMAL_PRIORITY,
      LOW_PRIORITY
   }


   public void testContainer() throws Exception {
      TreeStrategy strategy = new TreeStrategy("type", "length");
      Persister persister = new Persister(strategy);
      Container container = persister.read(ModuleContainer.class, TEXT);
      Service service = container.lookup("Service");
      ServiceFactory serviceFactory = container.lookup(ServiceFactory.class);

      assertNotNull(service);
      assertNotNull(service.startDate);
      assertNotNull(service.servicePriority);
      assertEquals(service.servicePriority, ServicePriority.LOW_PRIORITY);
      assertFalse(service.started);
      assertFalse(service.stopped);
      assertNotNull(serviceFactory);
      assertEquals(serviceFactory.service, service);
      assertEquals(service, container.lookup("Service"));
      assertEquals(serviceFactory.service, container.lookup(Service.class));
      assertEquals(serviceFactory.createCount.get(), 1);

      container.startAll();

      assertTrue(service.started);
      assertFalse(service.stopped);

      container.stopAll();

      assertTrue(service.started);
      assertTrue(service.stopped);

      Widget widget1 = container.lookup("Widget1");
      Widget widget2 = container.lookup("Widget2");

      assertNotNull(widget1);
      assertNotNull(widget2);
      assertEquals(widget1.widgetDate, widget2.widgetDate);

      boolean failedWithException = false;

      try {
         container.lookup(Widget.class);
      } catch(Exception e) {
         failedWithException = true;
         e.printStackTrace();
      }
      assertTrue("Looking up by type where there are multiple components of that type should result in exception", failedWithException);
   }
}
